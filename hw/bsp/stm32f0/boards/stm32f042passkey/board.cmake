set(MCU_VARIANT stm32f042x6)
set(JLINK_DEVICE stm32f042f6)
set(PYOCD_TARGET stm32f0x)

set(LD_FILE_GNU ${CMAKE_CURRENT_LIST_DIR}/STM32F042F6PX_FLASH.ld)

function(update_board TARGET)
  target_compile_definitions(${TARGET} PUBLIC
    STM32F042x6
    )
endfunction()
