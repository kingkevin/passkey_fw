MCU_VARIANT = stm32f042x6

CFLAGS += -DSTM32F042x6

# Linker
LD_FILE_GCC = $(BOARD_PATH)/STM32F042F6PX_FLASH.ld

# For flash-jlink target
JLINK_DEVICE = stm32f042f6
PYOCD_TARGET = stm32f0x

# flash target using on-board stlink
#flash: flash-stlink

OOCD ?= openocd
OOCD_INTERFACE ?= cmsis-dap
OOCD_TARGET ?= stm32f0x

flash: $(BUILD)/$(PROJECT).elf
	$(OOCD) -f interface/$(OOCD_INTERFACE).cfg \
		-f target/$(OOCD_TARGET).cfg \
		-c "program $< verify reset exit"

debug: $(BUILD)/$(PROJECT).elf
	$(GDB) --eval-command='target remote | $(OOCD) --file interface/$(OOCD_INTERFACE).cfg --command "transport select swd" --file target/$(OOCD_TARGET).cfg --command "gdb_port pipe; log_output /dev/null; init"' $<

dfu: $(BUILD)/$(PROJECT).bin
	dfu-util --device 0483:df11 --cfg 1 --intf 0 --alt 0 --detach --dfuse-address 0x08000000 --download $<
