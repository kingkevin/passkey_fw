/*
from https://github.com/NicoHood/HID/blob/master/src/KeyboardLayouts/

Copyright (c) 2014-2015 NicoHood
See the readme for credit to other people.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Include guard
#pragma once

// Keyboard Modifiers
enum KeyboardMods : uint16_t {
    MOD_LEFT_CTRL       = (1 <<  8),
    MOD_LEFT_SHIFT      = (1 <<  9),
    MOD_LEFT_ALT        = (1 << 10),
    MOD_LEFT_GUI        = (1 << 11),
    MOD_RIGHT_CTRL      = (1 << 12),
    MOD_RIGHT_SHIFT     = (1 << 13),
    MOD_RIGHT_ALT       = (1 << 14),
    MOD_RIGHT_GUI       = (uint16_t)(1 << 15),
};

// Keyboard Leds
enum KeyboardLeds : uint8_t {
    LED_NUM_LOCK        = (1 << 0),
    LED_CAPS_LOCK       = (1 << 1),
    LED_SCROLL_LOCK     = (1 << 2),
    LED_COMPOSE         = (1 << 3),
    LED_KANA            = (1 << 4),
    LED_POWER           = (1 << 5),
    LED_SHIFT           = (1 << 6),
    LED_DO_NOT_DISTURB  = (1 << 7),
};

// Hut1_12v2.pdf

enum KeyboardKeycode : uint8_t {
    KEY_RESERVED        =  0,
    KEY_ERROR_ROLLOVER  =  1,
    KEY_POST_FAIL       =  2,
    KEY_ERROR_UNDEFINED =  3,
    KEY_A               =  4,
    KEY_B               =  5,
    KEY_C               =  6,
    KEY_D               =  7,
    KEY_E               =  8,
    KEY_F               =  9,
    KEY_G               = 10,
    KEY_H               = 11,
    KEY_I               = 12,
    KEY_J               = 13,
    KEY_K               = 14,
    KEY_L               = 15,
    KEY_M               = 16,
    KEY_N               = 17,
    KEY_O               = 18,
    KEY_P               = 19,
    KEY_Q               = 20,
    KEY_R               = 21,
    KEY_S               = 22,
    KEY_T               = 23,
    KEY_U               = 24,
    KEY_V               = 25,
    KEY_W               = 26,
    KEY_X               = 27,
    KEY_Y               = 28,
    KEY_Z               = 29,
    KEY_1               = 30,
    KEY_2               = 31,
    KEY_3               = 32,
    KEY_4               = 33,
    KEY_5               = 34,
    KEY_6               = 35,
    KEY_7               = 36,
    KEY_8               = 37,
    KEY_9               = 38,
    KEY_0               = 39,
    KEY_ENTER           = 40,
    KEY_RETURN          = 40, // Alias
    KEY_ESC             = 41,
    KEY_BACKSPACE       = 42,
    KEY_TAB             = 43,
    KEY_SPACE           = 44,
    KEY_MINUS           = 45,
    KEY_EQUAL           = 46,
    KEY_LEFT_BRACE      = 47,
    KEY_RIGHT_BRACE     = 48,
    KEY_BACKSLASH       = 49,
    KEY_NON_US_NUM      = 50,
    KEY_SEMICOLON       = 51,
    KEY_QUOTE           = 52,
    KEY_TILDE           = 53,
    KEY_COMMA           = 54,
    KEY_PERIOD          = 55,
    KEY_SLASH           = 56,
    KEY_CAPS_LOCK       = 0x39,
    KEY_F1              = 0x3A,
    KEY_F2              = 0x3B,
    KEY_F3              = 0x3C,
    KEY_F4              = 0x3D,
    KEY_F5              = 0x3E,
    KEY_F6              = 0x3F,
    KEY_F7              = 0x40,
    KEY_F8              = 0x41,
    KEY_F9              = 0x42,
    KEY_F10             = 0x43,
    KEY_F11             = 0x44,
    KEY_F12             = 0x45,
    KEY_PRINT           = 0x46,
    KEY_PRINTSCREEN     = 0x46, // Alias
    KEY_SCROLL_LOCK     = 0x47,
    KEY_PAUSE           = 0x48,
    KEY_INSERT          = 0x49,
    KEY_HOME            = 0x4A,
    KEY_PAGE_UP         = 0x4B,
    KEY_DELETE          = 0x4C,
    KEY_END             = 0x4D,
    KEY_PAGE_DOWN       = 0x4E,
    KEY_RIGHT_ARROW     = 0x4F,
    KEY_LEFT_ARROW      = 0x50,
    KEY_DOWN_ARROW      = 0x51,
    KEY_UP_ARROW        = 0x52,
    KEY_RIGHT           = 0x4F, // Alias
    KEY_LEFT            = 0x50, // Alias
    KEY_DOWN            = 0x51, // Alias
    KEY_UP              = 0x52, // Alias
    KEY_NUM_LOCK        = 0x53,
    KEYPAD_DIVIDE       = 0x54,
    KEYPAD_MULTIPLY     = 0x55,
    KEYPAD_SUBTRACT     = 0x56,
    KEYPAD_ADD          = 0x57,
    KEYPAD_ENTER        = 0x58,
    KEYPAD_1            = 0x59,
    KEYPAD_2            = 0x5A,
    KEYPAD_3            = 0x5B,
    KEYPAD_4            = 0x5C,
    KEYPAD_5            = 0x5D,
    KEYPAD_6            = 0x5E,
    KEYPAD_7            = 0x5F,
    KEYPAD_8            = 0x60,
    KEYPAD_9            = 0x61,
    KEYPAD_0            = 0x62,
    KEYPAD_DOT          = 0x63,
    KEY_NON_US          = 0x64,
    KEY_APPLICATION     = 0x65, // Context menu/right click
    KEY_MENU            = 0x65, // Alias

    // Most of the following keys will only work with Linux or not at all.
    // F13+ keys are mostly used for laptop functions like ECO key.
    KEY_POWER           = 0x66, // PowerOff (Ubuntu)
    KEY_PAD_EQUALS      = 0x67, // Dont confuse with KEYPAD_EQUAL_SIGN
    KEY_F13             = 0x68, // Tools (Ubunutu)
    KEY_F14             = 0x69, // Launch5 (Ubuntu)
    KEY_F15             = 0x6A, // Launch6 (Ubuntu)
    KEY_F16             = 0x6B, // Launch7 (Ubuntu)
    KEY_F17             = 0x6C, // Launch8 (Ubuntu)
    KEY_F18             = 0x6D, // Launch9 (Ubuntu)
    KEY_F19             = 0x6E, // Disabled (Ubuntu)
    KEY_F20             = 0x6F, // AudioMicMute (Ubuntu)
    KEY_F21             = 0x70, // Touchpad toggle (Ubuntu)
    KEY_F22             = 0x71, // TouchpadOn (Ubuntu)
    KEY_F23             = 0x72, // TouchpadOff Ubuntu)
    KEY_F24             = 0x73, // Disabled (Ubuntu)
    KEY_EXECUTE         = 0x74, // Open (Ubuntu)
    KEY_HELP            = 0x75, // Help (Ubuntu)
    KEY_MENU2           = 0x76, // Disabled (Ubuntu)
    KEY_SELECT          = 0x77, // Disabled (Ubuntu)
    KEY_STOP            = 0x78, // Cancel (Ubuntu)
    KEY_AGAIN           = 0x79, // Redo (Ubuntu)
    KEY_UNDO            = 0x7A, // Undo (Ubuntu)
    KEY_CUT             = 0x7B, // Cut (Ubuntu)
    KEY_COPY            = 0x7C, // Copy (Ubuntu)
    KEY_PASTE           = 0x7D, // Paste (Ubuntu)
    KEY_FIND            = 0x7E, // Find (Ubuntu)
    KEY_MUTE            = 0x7F,
    KEY_VOLUME_MUTE     = 0x7F, // Alias
    KEY_VOLUME_UP       = 0x80,
    KEY_VOLUME_DOWN     = 0x81,
    KEY_LOCKING_CAPS_LOCK   = 0x82, // Disabled (Ubuntu)
    KEY_LOCKING_NUM_LOCK    = 0x83, // Disabled (Ubuntu)
    KEY_LOCKING_SCROLL_LOCK = 0x84, // Disabled (Ubuntu)
    KEYPAD_COMMA            = 0x85, // .
    KEYPAD_EQUAL_SIGN       = 0x86, // Disabled (Ubuntu), Dont confuse with KEYPAD_EQUAL
    KEY_INTERNATIONAL1      = 0x87, // Disabled (Ubuntu)
    KEY_INTERNATIONAL2      = 0x88, // Hiragana Katakana (Ubuntu)
    KEY_INTERNATIONAL3      = 0x89, // Disabled (Ubuntu)
    KEY_INTERNATIONAL4      = 0x8A, // Henkan (Ubuntu)
    KEY_INTERNATIONAL5      = 0x8B, // Muhenkan (Ubuntu)
    KEY_INTERNATIONAL6      = 0x8C, // Disabled (Ubuntu)
    KEY_INTERNATIONAL7      = 0x8D, // Disabled (Ubuntu)
    KEY_INTERNATIONAL8      = 0x8E, // Disabled (Ubuntu)
    KEY_INTERNATIONAL9      = 0x8F, // Disabled (Ubuntu)
    KEY_LANG1               = 0x90, // Disabled (Ubuntu)
    KEY_LANG2               = 0x91, // Disabled (Ubuntu)
    KEY_LANG3               = 0x92, // Katakana (Ubuntu)
    KEY_LANG4               = 0x93, // Hiragana (Ubuntu)
    KEY_LANG5               = 0x94, // Disabled (Ubuntu)
    KEY_LANG6               = 0x95, // Disabled (Ubuntu)
    KEY_LANG7               = 0x96, // Disabled (Ubuntu)
    KEY_LANG8               = 0x97, // Disabled (Ubuntu)
    KEY_LANG9               = 0x98, // Disabled (Ubuntu)
    KEY_ALTERNATE_ERASE     = 0x99, // Disabled (Ubuntu)
    KEY_SYSREQ_ATTENTION    = 0x9A, // Disabled (Ubuntu)
    KEY_CANCEL              = 0x9B, // Disabled (Ubuntu)
    KEY_CLEAR               = 0x9C, // Delete (Ubuntu)
    KEY_PRIOR               = 0x9D, // Disabled (Ubuntu)
    KEY_RETURN2             = 0x9E, // Disabled (Ubuntu), Do not confuse this with KEY_ENTER
    KEY_SEPARATOR           = 0x9F, // Disabled (Ubuntu)
    KEY_OUT                 = 0xA0, // Disabled (Ubuntu)
    KEY_OPER                = 0xA1, // Disabled (Ubuntu)
    KEY_CLEAR_AGAIN         = 0xA2, // Disabled (Ubuntu)
    KEY_CRSEL_PROPS         = 0xA3, // Disabled (Ubuntu)
    KEY_EXSEL               = 0xA4, // Disabled (Ubuntu)

    KEY_PAD_00                  = 0xB0, // Disabled (Ubuntu)
    KEY_PAD_000                 = 0xB1, // Disabled (Ubuntu)
    KEY_THOUSANDS_SEPARATOR     = 0xB2, // Disabled (Ubuntu)
    KEY_DECIMAL_SEPARATOR       = 0xB3, // Disabled (Ubuntu)
    KEY_CURRENCY_UNIT           = 0xB4, // Disabled (Ubuntu)
    KEY_CURRENCY_SUB_UNIT       = 0xB5, // Disabled (Ubuntu)
    KEYPAD_LEFT_BRACE           = 0xB6, // (
    KEYPAD_RIGHT_BRACE          = 0xB7, // )
    KEYPAD_LEFT_CURLY_BRACE     = 0xB8, // Disabled (Ubuntu)
    KEYPAD_RIGHT_CURLY_BRACE    = 0xB9, // Disabled (Ubuntu)
    KEYPAD_TAB                  = 0xBA, // Disabled (Ubuntu)
    KEYPAD_BACKSPACE            = 0xBB, // Disabled (Ubuntu)
    KEYPAD_A                    = 0xBC, // Disabled (Ubuntu)
    KEYPAD_B                    = 0xBD, // Disabled (Ubuntu)
    KEYPAD_C                    = 0xBE, // Disabled (Ubuntu)
    KEYPAD_D                    = 0xBF, // Disabled (Ubuntu)
    KEYPAD_E                    = 0xC0, // Disabled (Ubuntu)
    KEYPAD_F                    = 0xC1, // Disabled (Ubuntu)
    KEYPAD_XOR                  = 0xC2, // Disabled (Ubuntu)
    KEYPAD_CARET                = 0xC3, // Disabled (Ubuntu)
    KEYPAD_PERCENT              = 0xC4, // Disabled (Ubuntu)
    KEYPAD_LESS_THAN            = 0xC5, // Disabled (Ubuntu)
    KEYPAD_GREATER_THAN         = 0xC6, // Disabled (Ubuntu)
    KEYPAD_AMPERSAND            = 0xC7, // Disabled (Ubuntu)
    KEYPAD_DOUBLEAMPERSAND      = 0xC8, // Disabled (Ubuntu)
    KEYPAD_PIPE                 = 0xC9, // Disabled (Ubuntu)
    KEYPAD_DOUBLEPIPE           = 0xCA, // Disabled (Ubuntu)
    KEYPAD_COLON                = 0xCB, // Disabled (Ubuntu)
    KEYPAD_POUND_SIGN           = 0xCC, // Disabled (Ubuntu)
    KEYPAD_SPACE                = 0xCD, // Disabled (Ubuntu)
    KEYPAD_AT_SIGN              = 0xCE, // Disabled (Ubuntu)
    KEYPAD_EXCLAMATION_POINT    = 0xCF, // Disabled (Ubuntu)
    KEYPAD_MEMORY_STORE         = 0xD0, // Disabled (Ubuntu)
    KEYPAD_MEMORY_RECALL        = 0xD1, // Disabled (Ubuntu)
    KEYPAD_MEMORY_CLEAR         = 0xD2, // Disabled (Ubuntu)
    KEYPAD_MEMORY_ADD           = 0xD3, // Disabled (Ubuntu)
    KEYPAD_MEMORY_SUBTRACT      = 0xD4, // Disabled (Ubuntu)
    KEYPAD_MEMORY_MULTIPLY      = 0xD5, // Disabled (Ubuntu)
    KEYPAD_MEMORY_DIVIDE        = 0xD6, // Disabled (Ubuntu)
    KEYPAD_PLUS_MINUS           = 0xD7, // Disabled (Ubuntu)
    KEYPAD_CLEAR                = 0xD8, // Delete (Ubuntu)
    KEYPAD_CLEAR_ENTRY          = 0xD9, // Disabled (Ubuntu)
    KEYPAD_BINARY               = 0xDA, // Disabled (Ubuntu)
    KEYPAD_OCTAL                = 0xDB, // Disabled (Ubuntu)
    KEYPAD_DECIMAL              = 0xDC, // Disabled (Ubuntu)
    KEYPAD_HEXADECIMAL          = 0xDD, // Disabled (Ubuntu)

    KEY_LEFT_CTRL           = 0xE0,
    KEY_LEFT_SHIFT          = 0xE1,
    KEY_LEFT_ALT            = 0xE2,
    KEY_LEFT_GUI            = 0xE3,
    KEY_LEFT_WINDOWS        = 0xE3, // Alias
    KEY_RIGHT_CTRL          = 0xE4,
    KEY_RIGHT_SHIFT         = 0xE5,
    KEY_RIGHT_ALT           = 0xE6,
    KEY_RIGHT_GUI           = 0xE7,
    KEY_RIGHT_WINDOWS       = 0xE7, // Alias

    // Keyboard HID mappings

    //  Reserved (no_event_indicated)
    HID_KEYBOARD_ERROR_ROLLOVER = 0x01,
    HID_KEYBOARD_POST_FAIL  = 0x02,
    HID_KEYBOARD_ERROR_UNDEFINED    = 0x03,
    HID_KEYBOARD_A_AND_A    = 0x04,
    HID_KEYBOARD_B_AND_B    = 0x05,
    HID_KEYBOARD_C_AND_C    = 0x06,
    HID_KEYBOARD_D_AND_D    = 0x07,
    HID_KEYBOARD_E_AND_E    = 0x08,
    HID_KEYBOARD_F_AND_F    = 0x09,
    HID_KEYBOARD_G_AND_G    = 0x0A,
    HID_KEYBOARD_H_AND_H    = 0x0B,
    HID_KEYBOARD_I_AND_I    = 0x0C,
    HID_KEYBOARD_J_AND_J    = 0x0D,
    HID_KEYBOARD_K_AND_K    = 0x0E,
    HID_KEYBOARD_L_AND_L    = 0x0F,
    HID_KEYBOARD_M_AND_M    = 0x10,
    HID_KEYBOARD_N_AND_N    = 0x11,
    HID_KEYBOARD_O_AND_O    = 0x12,
    HID_KEYBOARD_P_AND_P    = 0x13,
    HID_KEYBOARD_Q_AND_Q    = 0x14,
    HID_KEYBOARD_R_AND_R    = 0x15,
    HID_KEYBOARD_S_AND_S    = 0x16,
    HID_KEYBOARD_T_AND_T    = 0x17,
    HID_KEYBOARD_U_AND_U    = 0x18,
    HID_KEYBOARD_V_AND_V    = 0x19,
    HID_KEYBOARD_W_AND_W    = 0x1A,
    HID_KEYBOARD_X_AND_X    = 0x1B,
    HID_KEYBOARD_Y_AND_Y    = 0x1C,
    HID_KEYBOARD_Z_AND_Z    = 0x1D,
    HID_KEYBOARD_1_AND_EXCLAMATION_POINT    = 0x1E,
    HID_KEYBOARD_2_AND_AT   = 0x1F,
    HID_KEYBOARD_3_AND_POUND    = 0x20,
    HID_KEYBOARD_4_AND_DOLLAR   = 0x21,
    HID_KEYBOARD_5_AND_PERCENT  = 0x22,
    HID_KEYBOARD_6_AND_CARAT    = 0x23,
    HID_KEYBOARD_7_AND_AMPERSAND    = 0x24,
    HID_KEYBOARD_8_AND_ASTERISK = 0x25,
    HID_KEYBOARD_9_AND_LEFT_PAREN   = 0x26,
    HID_KEYBOARD_0_AND_RIGHT_PAREN  = 0x27,
    HID_KEYBOARD_ENTER  = 0x28,  // (MARKED AS ENTER_SLASH_RETURN)
    HID_KEYBOARD_ESCAPE = 0x29,
    HID_KEYBOARD_DELETE = 0x2A, // (BACKSPACE)
    HID_KEYBOARD_TAB    = 0x2B,
    HID_KEYBOARD_SPACEBAR   = 0x2C,
    HID_KEYBOARD_MINUS_AND_UNDERSCORE   = 0x2D,  // (UNDERSCORE)
    HID_KEYBOARD_EQUALS_AND_PLUS    = 0x2E,
    HID_KEYBOARD_LEFT_BRACKET_AND_LEFT_CURLY_BRACE  = 0x2F,
    HID_KEYBOARD_RIGHT_BRACKET_AND_RIGHT_CURLY_BRACE    = 0x30,
    HID_KEYBOARD_BACKSLASH_AND_PIPE = 0x31,
    HID_KEYBOARD_NON_US_POUND_AND_TILDE = 0x32,
    HID_KEYBOARD_SEMICOLON_AND_COLON    = 0x33,
    HID_KEYBOARD_QUOTE_AND_DOUBLEQUOTE  = 0x34,
    HID_KEYBOARD_GRAVE_ACCENT_AND_TILDE = 0x35,
    HID_KEYBOARD_COMMA_AND_LESS_THAN    = 0x36,
    HID_KEYBOARD_PERIOD_AND_GREATER_THAN    = 0x37,
    HID_KEYBOARD_SLASH_AND_QUESTION_MARK    = 0x38,
    HID_KEYBOARD_CAPS_LOCK  = 0x39,
    HID_KEYBOARD_F1 = 0x3A,
    HID_KEYBOARD_F2 = 0x3B,
    HID_KEYBOARD_F3 = 0x3C,
    HID_KEYBOARD_F4 = 0x3D,
    HID_KEYBOARD_F5 = 0x3E,
    HID_KEYBOARD_F6 = 0x3F,
    HID_KEYBOARD_F7 = 0x40,
    HID_KEYBOARD_F8 = 0x41,
    HID_KEYBOARD_F9 = 0x42,
    HID_KEYBOARD_F10    = 0x43,
    HID_KEYBOARD_F11    = 0x44,
    HID_KEYBOARD_F12    = 0x45,
    HID_KEYBOARD_PRINTSCREEN    = 0x46,
    HID_KEYBOARD_SCROLL_LOCK    = 0x47,
    HID_KEYBOARD_PAUSE  = 0x48,
    HID_KEYBOARD_INSERT = 0x49,
    HID_KEYBOARD_HOME   = 0x4A,
    HID_KEYBOARD_PAGE_UP    = 0x4B,
    HID_KEYBOARD_DELETE_FORWARD = 0x4C,
    HID_KEYBOARD_END    = 0x4D,
    HID_KEYBOARD_PAGE_DOWN  = 0x4E,
    HID_KEYBOARD_RIGHTARROW = 0x4F,
    HID_KEYBOARD_LEFTARROW  = 0x50,
    HID_KEYBOARD_DOWNARROW  = 0x51,
    HID_KEYBOARD_UPARROW    = 0x52,
    HID_KEYPAD_NUM_LOCK_AND_CLEAR   = 0x53,
    HID_KEYPAD_DIVIDE   = 0x54,
    HID_KEYPAD_MULTIPLY = 0x55,
    HID_KEYPAD_SUBTRACT = 0x56,
    HID_KEYPAD_ADD  = 0x57,
    HID_KEYPAD_ENTER    = 0x58,
    HID_KEYPAD_1_AND_END    = 0x59,
    HID_KEYPAD_2_AND_DOWN_ARROW = 0x5A,
    HID_KEYPAD_3_AND_PAGE_DOWN  = 0x5B,
    HID_KEYPAD_4_AND_LEFT_ARROW = 0x5C,
    HID_KEYPAD_5    = 0x5D,
    HID_KEYPAD_6_AND_RIGHT_ARROW    = 0x5E,
    HID_KEYPAD_7_AND_HOME   = 0x5F,
    HID_KEYPAD_8_AND_UP_ARROW   = 0x60,
    HID_KEYPAD_9_AND_PAGE_UP    = 0x61,
    HID_KEYPAD_0_AND_INSERT = 0x62,
    HID_KEYPAD_PERIOD_AND_DELETE    = 0x63,
    HID_KEYBOARD_NON_US_BACKSLASH_AND_PIPE  = 0x64,
    HID_KEYBOARD_APPLICATION    = 0x65,
    HID_KEYBOARD_POWER  = 0x66,
    HID_KEYPAD_EQUALS   = 0x67,
    HID_KEYBOARD_F13    = 0x68,
    HID_KEYBOARD_F14    = 0x69,
    HID_KEYBOARD_F15    = 0x6A,
    HID_KEYBOARD_F16    = 0x6B,
    HID_KEYBOARD_F17    = 0x6C,
    HID_KEYBOARD_F18    = 0x6D,
    HID_KEYBOARD_F19    = 0x6E,
    HID_KEYBOARD_F20    = 0x6F,
    HID_KEYBOARD_F21    = 0x70,
    HID_KEYBOARD_F22    = 0x71,
    HID_KEYBOARD_F23    = 0x72,
    HID_KEYBOARD_F24    = 0x73,
    HID_KEYBOARD_EXECUTE    = 0x74,
    HID_KEYBOARD_HELP   = 0x75,
    HID_KEYBOARD_MENU   = 0x76,
    HID_KEYBOARD_SELECT = 0x77,
    HID_KEYBOARD_STOP   = 0x78,
    HID_KEYBOARD_AGAIN  = 0x79,
    HID_KEYBOARD_UNDO   = 0x7A,
    HID_KEYBOARD_CUT    = 0x7B,
    HID_KEYBOARD_COPY   = 0x7C,
    HID_KEYBOARD_PASTE  = 0x7D,
    HID_KEYBOARD_FIND   = 0x7E,
    HID_KEYBOARD_MUTE   = 0x7F,
    HID_KEYBOARD_VOLUME_UP  = 0x80,
    HID_KEYBOARD_VOLUME_DOWN    = 0x81,
    HID_KEYBOARD_LOCKING_CAPS_LOCK  = 0x82,
    HID_KEYBOARD_LOCKING_NUM_LOCK   = 0x83,
    HID_KEYBOARD_LOCKING_SCROLL_LOCK    = 0x84,
    HID_KEYPAD_COMMA    = 0x85,
    HID_KEYPAD_EQUAL_SIGN   = 0x86,
    HID_KEYBOARD_INTERNATIONAL1 = 0x87,
    HID_KEYBOARD_INTERNATIONAL2 = 0x88,
    HID_KEYBOARD_INTERNATIONAL3 = 0x89,
    HID_KEYBOARD_INTERNATIONAL4 = 0x8A,
    HID_KEYBOARD_INTERNATIONAL5 = 0x8B,
    HID_KEYBOARD_INTERNATIONAL6 = 0x8C,
    HID_KEYBOARD_INTERNATIONAL7 = 0x8D,
    HID_KEYBOARD_INTERNATIONAL8 = 0x8E,
    HID_KEYBOARD_INTERNATIONAL9 = 0x8F,
    HID_KEYBOARD_LANG1  = 0x90,
    HID_KEYBOARD_LANG2  = 0x91,
    HID_KEYBOARD_LANG3  = 0x92,
    HID_KEYBOARD_LANG4  = 0x93,
    HID_KEYBOARD_LANG5  = 0x94,
    HID_KEYBOARD_LANG6  = 0x95,
    HID_KEYBOARD_LANG7  = 0x96,
    HID_KEYBOARD_LANG8  = 0x97,
    HID_KEYBOARD_LANG9  = 0x98,
    HID_KEYBOARD_ALTERNATE_ERASE    = 0x99,
    HID_KEYBOARD_SYSREQ_SLASH_ATTENTION = 0x9A,
    HID_KEYBOARD_CANCEL = 0x9B,
    HID_KEYBOARD_CLEAR  = 0x9C,
    HID_KEYBOARD_PRIOR  = 0x9D,
    HID_KEYBOARD_RETURN = 0x9E,
    HID_KEYBOARD_SEPARATOR  = 0x9F,
    HID_KEYBOARD_OUT    = 0xA0,
    HID_KEYBOARD_OPER   = 0xA1,
    HID_KEYBOARD_CLEAR_SLASH_AGAIN  = 0xA2,
    HID_KEYBOARD_CRSEL_SLASH_PROPS  = 0xA3,
    HID_KEYBOARD_EXSEL  = 0xA4,
    // Reserved 0xA5-AF
    HID_KEYPAD_00   = 0xB0,
    HID_KEYPAD_000  = 0xB1,
    HID_THOUSANDS_SEPARATOR = 0xB2,
    HID_DECIMAL_SEPARATOR   = 0xB3,
    HID_CURRENCY_UNIT   = 0xB4,
    HID_CURRENCY_SUBUNIT    = 0xB5,
    HID_KEYPAD_LEFT_PAREN   = 0xB6,
    HID_KEYPAD_RIGHT_PAREN  = 0xB7,
    HID_KEYPAD_LEFT_CURLY_BRACE = 0xB8,
    HID_KEYPAD_RIGHT_CURLY_BRACE    = 0xB9,
    HID_KEYPAD_TAB  = 0xBA,
    HID_KEYPAD_BACKSPACE    = 0xBB,
    HID_KEYPAD_A    = 0xBC,
    HID_KEYPAD_B    = 0xBD,
    HID_KEYPAD_C    = 0xBE,
    HID_KEYPAD_D    = 0xBF,
    HID_KEYPAD_E    = 0xC0,
    HID_KEYPAD_F    = 0xC1,
    HID_KEYPAD_XOR  = 0xC2,
    HID_KEYPAD_CARAT    = 0xC3,
    HID_KEYPAD_PERCENT  = 0xC4,
    HID_KEYPAD_LESS_THAN    = 0xC5,
    HID_KEYPAD_GREATER_THAN = 0xC6,
    HID_KEYPAD_AMPERSAND    = 0xC7,
    HID_KEYPAD_DOUBLEAMPERSAND  = 0xC8,
    HID_KEYPAD_PIPE = 0xC9,
    HID_KEYPAD_DOUBLEPIPE   = 0xCA,
    HID_KEYPAD_COLON    = 0xCB,
    HID_KEYPAD_POUND_SIGN   = 0xCC,
    HID_KEYPAD_SPACE    = 0xCD,
    HID_KEYPAD_AT_SIGN  = 0xCE,
    HID_KEYPAD_EXCLAMATION_POINT    = 0xCF,
    HID_KEYPAD_MEMORY_STORE = 0xD0,
    HID_KEYPAD_MEMORY_RECALL    = 0xD1,
    HID_KEYPAD_MEMORY_CLEAR = 0xD2,
    HID_KEYPAD_MEMORY_ADD   = 0xD3,
    HID_KEYPAD_MEMORY_SUBTRACT  = 0xD4,
    HID_KEYPAD_MEMORY_MULTIPLY  = 0xD5,
    HID_KEYPAD_MEMORY_DIVIDE    = 0xD6,
    HID_KEYPAD_PLUS_SLASH_MINUS = 0xD7,
    HID_KEYPAD_CLEAR    = 0xD8,
    HID_KEYPAD_CLEAR_ENTRY  = 0xD9,
    HID_KEYPAD_BINARY   = 0xDA,
    HID_KEYPAD_OCTAL    = 0xDB,
    HID_KEYPAD_DECIMAL  = 0xDC,
    HID_KEYPAD_HEXADECIMAL  = 0xDD,

    //  0xDE-0xDF    - RESERVED
    HID_KEYBOARD_LEFT_CONTROL   = 0xE0,
    HID_KEYBOARD_LEFT_SHIFT = 0xE1,
    HID_KEYBOARD_LEFT_ALT   = 0xE2,
    HID_KEYBOARD_LEFT_GUI   = 0xE3,
    HID_KEYBOARD_RIGHT_CONTROL  = 0xE4,
    HID_KEYBOARD_RIGHT_SHIFT    = 0xE5,
    HID_KEYBOARD_RIGHT_ALT  = 0xE6,
    HID_KEYBOARD_RIGHT_GUI  = 0xE7,
};

static const uint16_t us_asciimap[96] =
{
    KEY_SPACE,                          // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,               // !
    KEY_QUOTE|MOD_LEFT_SHIFT,           // "
    KEY_3|MOD_LEFT_SHIFT,               // #
    KEY_4|MOD_LEFT_SHIFT,               // $
    KEY_5|MOD_LEFT_SHIFT,               // %
    KEY_7|MOD_LEFT_SHIFT,               // &
    KEY_QUOTE,                          // '
    KEY_9|MOD_LEFT_SHIFT,               // (
    KEY_0|MOD_LEFT_SHIFT,               // )
    KEY_8|MOD_LEFT_SHIFT,               // *
    KEY_EQUAL|MOD_LEFT_SHIFT,           // +
    KEY_COMMA,                          // ,
    KEY_MINUS,                          // -
    KEY_PERIOD,                         // .
    KEY_SLASH,                          // /
    KEY_0,                              // 0
    KEY_1,                              // 1
    KEY_2,                              // 2
    KEY_3,                              // 3
    KEY_4,                              // 4
    KEY_5,                              // 5
    KEY_6,                              // 6
    KEY_7,                              // 7
    KEY_8,                              // 8
    KEY_9,                              // 9
    KEY_SEMICOLON|MOD_LEFT_SHIFT,       // :
    KEY_SEMICOLON,                      // ;
    KEY_COMMA|MOD_LEFT_SHIFT,           // <
    KEY_EQUAL,                          // =
    KEY_PERIOD|MOD_LEFT_SHIFT,          // >
    KEY_SLASH|MOD_LEFT_SHIFT,           // ?
    KEY_2|MOD_LEFT_SHIFT,               // @
    KEY_A|MOD_LEFT_SHIFT,               // A
    KEY_B|MOD_LEFT_SHIFT,               // B
    KEY_C|MOD_LEFT_SHIFT,               // C
    KEY_D|MOD_LEFT_SHIFT,               // D
    KEY_E|MOD_LEFT_SHIFT,               // E
    KEY_F|MOD_LEFT_SHIFT,               // F
    KEY_G|MOD_LEFT_SHIFT,               // G
    KEY_H|MOD_LEFT_SHIFT,               // H
    KEY_I|MOD_LEFT_SHIFT,               // I
    KEY_J|MOD_LEFT_SHIFT,               // J
    KEY_K|MOD_LEFT_SHIFT,               // K
    KEY_L|MOD_LEFT_SHIFT,               // L
    KEY_M|MOD_LEFT_SHIFT,               // M
    KEY_N|MOD_LEFT_SHIFT,               // N
    KEY_O|MOD_LEFT_SHIFT,               // O
    KEY_P|MOD_LEFT_SHIFT,               // P
    KEY_Q|MOD_LEFT_SHIFT,               // Q
    KEY_R|MOD_LEFT_SHIFT,               // R
    KEY_S|MOD_LEFT_SHIFT,               // S
    KEY_T|MOD_LEFT_SHIFT,               // T
    KEY_U|MOD_LEFT_SHIFT,               // U
    KEY_V|MOD_LEFT_SHIFT,               // V
    KEY_W|MOD_LEFT_SHIFT,               // W
    KEY_X|MOD_LEFT_SHIFT,               // X
    KEY_Y|MOD_LEFT_SHIFT,               // Y
    KEY_Z|MOD_LEFT_SHIFT,               // Z
    KEY_LEFT_BRACE,                     // [
    KEY_BACKSLASH,                      // bslash
    KEY_RIGHT_BRACE,                    // ]
    KEY_6|MOD_LEFT_SHIFT,               // ^
    KEY_MINUS|MOD_LEFT_SHIFT,           // _
    KEY_TILDE,                          // `
    KEY_A,                              // a
    KEY_B,                              // b
    KEY_C,                              // c
    KEY_D,                              // d
    KEY_E,                              // e
    KEY_F,                              // f
    KEY_G,                              // g
    KEY_H,                              // h
    KEY_I,                              // i
    KEY_J,                              // j
    KEY_K,                              // k
    KEY_L,                              // l
    KEY_M,                              // m
    KEY_N,                              // n
    KEY_O,                              // o
    KEY_P,                              // p
    KEY_Q,                              // q
    KEY_R,                              // r
    KEY_S,                              // s
    KEY_T,                              // t
    KEY_U,                              // u
    KEY_V,                              // v
    KEY_W,                              // w
    KEY_X,                              // x
    KEY_Y,                              // y
    KEY_Z,                              // z
    KEY_LEFT_BRACE|MOD_LEFT_SHIFT,      // {
    KEY_BACKSLASH|MOD_LEFT_SHIFT,       // |
    KEY_RIGHT_BRACE|MOD_LEFT_SHIFT,     // }
    KEY_TILDE|MOD_LEFT_SHIFT,           // ~
    KEY_RESERVED,                       // DEL
    // 7-bit ASCII codes end here
};

// Key aliases
#define KEY_DK_FRACTION     KEY_TILDE
#define KEY_DK_PLUS         KEY_MINUS       // LOL! :)
#define KEY_DK_ACCENTS1     KEY_EQUAL
#define KEY_DK_ACIRCLE      KEY_LEFT_BRACE
#define KEY_DK_ACCENTS2     KEY_RIGHT_BRACE
#define KEY_DK_AE           KEY_SEMICOLON
#define KEY_DK_OSLASH       KEY_QUOTE
#define KEY_DK_QUOTE        KEY_BACKSLASH
#define KEY_DK_QUOTE        KEY_BACKSLASH
#define KEY_DK_MINUS        KEY_SLASH
#define KEY_DK_LT_GT        KEY_NON_US

static const uint16_t dk_asciimap[96] =
{
    KEY_SPACE,                                  // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,                       // !
    KEY_2|MOD_LEFT_SHIFT,                       // "
    KEY_3|MOD_LEFT_SHIFT,                       // #
    KEY_4|MOD_RIGHT_ALT,                        // $
    KEY_5|MOD_LEFT_SHIFT,                       // %
    KEY_6|MOD_LEFT_SHIFT,                       // &
    KEY_DK_QUOTE,                               // '
    KEY_8|MOD_LEFT_SHIFT,                       // (
    KEY_9|MOD_LEFT_SHIFT,                       // )
    KEY_DK_QUOTE|MOD_LEFT_SHIFT,                // *
    KEY_DK_PLUS,                                // +
    KEY_COMMA,                                  // ,
    KEY_DK_MINUS,                               // -
    KEY_PERIOD,                                 // .
    KEY_7|MOD_LEFT_SHIFT,                       // /
    KEY_0,                                      // 0
    KEY_1,                                      // 1
    KEY_2,                                      // 2
    KEY_3,                                      // 3
    KEY_4,                                      // 4
    KEY_5,                                      // 5
    KEY_6,                                      // 6
    KEY_7,                                      // 7
    KEY_8,                                      // 8
    KEY_9,                                      // 9
    KEY_PERIOD|MOD_LEFT_SHIFT,                  // :
    KEY_COMMA|MOD_LEFT_SHIFT,                   // ;
    KEY_DK_LT_GT,                               // <
    KEY_0|MOD_LEFT_SHIFT,                       // =
    KEY_DK_LT_GT|MOD_LEFT_SHIFT,                // >
    KEY_DK_PLUS|MOD_LEFT_SHIFT,                 // ?
    KEY_2|MOD_RIGHT_ALT,                        // @
    KEY_A|MOD_LEFT_SHIFT,                       // A
    KEY_B|MOD_LEFT_SHIFT,                       // B
    KEY_C|MOD_LEFT_SHIFT,                       // C
    KEY_D|MOD_LEFT_SHIFT,                       // D
    KEY_E|MOD_LEFT_SHIFT,                       // E
    KEY_F|MOD_LEFT_SHIFT,                       // F
    KEY_G|MOD_LEFT_SHIFT,                       // G
    KEY_H|MOD_LEFT_SHIFT,                       // H
    KEY_I|MOD_LEFT_SHIFT,                       // I
    KEY_J|MOD_LEFT_SHIFT,                       // J
    KEY_K|MOD_LEFT_SHIFT,                       // K
    KEY_L|MOD_LEFT_SHIFT,                       // L
    KEY_M|MOD_LEFT_SHIFT,                       // M
    KEY_N|MOD_LEFT_SHIFT,                       // N
    KEY_O|MOD_LEFT_SHIFT,                       // O
    KEY_P|MOD_LEFT_SHIFT,                       // P
    KEY_Q|MOD_LEFT_SHIFT,                       // Q
    KEY_R|MOD_LEFT_SHIFT,                       // R
    KEY_S|MOD_LEFT_SHIFT,                       // S
    KEY_T|MOD_LEFT_SHIFT,                       // T
    KEY_U|MOD_LEFT_SHIFT,                       // U
    KEY_V|MOD_LEFT_SHIFT,                       // V
    KEY_W|MOD_LEFT_SHIFT,                       // W
    KEY_X|MOD_LEFT_SHIFT,                       // X
    KEY_Y|MOD_LEFT_SHIFT,                       // Y
    KEY_Z|MOD_LEFT_SHIFT,                       // Z
    KEY_8|MOD_RIGHT_ALT,                        // [
    KEY_DK_LT_GT|MOD_RIGHT_ALT,                 // bslash
    KEY_9|MOD_RIGHT_ALT,                        // ]
    KEY_RESERVED,                               // ^ (Dead key)
    KEY_DK_MINUS|MOD_LEFT_SHIFT,                // _
    KEY_RESERVED,                               // ` (Dead key)
    KEY_A,                                      // a
    KEY_B,                                      // b
    KEY_C,                                      // c
    KEY_D,                                      // d
    KEY_E,                                      // e
    KEY_F,                                      // f
    KEY_G,                                      // g
    KEY_H,                                      // h
    KEY_I,                                      // i
    KEY_J,                                      // j
    KEY_K,                                      // k
    KEY_L,                                      // l
    KEY_M,                                      // m
    KEY_N,                                      // n
    KEY_O,                                      // o
    KEY_P,                                      // p
    KEY_Q,                                      // q
    KEY_R,                                      // r
    KEY_S,                                      // s
    KEY_T,                                      // t
    KEY_U,                                      // u
    KEY_V,                                      // v
    KEY_W,                                      // w
    KEY_X,                                      // x
    KEY_Y,                                      // y
    KEY_Z,                                      // z
    KEY_7|MOD_RIGHT_ALT,                        // {
    KEY_DK_ACCENTS1|MOD_RIGHT_ALT,              // |
    KEY_0|MOD_RIGHT_ALT,                        // }
    KEY_RESERVED,                               // ~ (Dead key)
    KEY_RESERVED,                               // 127 - DEL
    // 7-bit ASCII codes end here
};


// Key aliases
#define KEY_SE_SECTION      KEY_TILDE
#define KEY_SE_PLUS         KEY_MINUS       // LOL! :)
#define KEY_SE_BACKSLASH    KEY_EQUAL
#define KEY_SE_ACIRCLE      KEY_LEFT_BRACE
#define KEY_SE_ACCENTS2     KEY_RIGHT_BRACE
#define KEY_SE_AUMLAUT      KEY_QUOTE
#define KEY_SE_OUMLAUT      KEY_SEMICOLON
#define KEY_SE_QUOTE        KEY_BACKSLASH
#define KEY_SE_MINUS        KEY_SLASH
#define KEY_SE_LT_GT        KEY_NON_US

static const uint16_t se_asciimap[] =
{
    KEY_SPACE,                                  // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,                       // !
    KEY_2|MOD_LEFT_SHIFT,                       // "
    KEY_3|MOD_LEFT_SHIFT,                       // #
    KEY_4|MOD_RIGHT_ALT,                        // $
    KEY_5|MOD_LEFT_SHIFT,                       // %
    KEY_6|MOD_LEFT_SHIFT,                       // &
    KEY_SE_QUOTE,                               // '
    KEY_8|MOD_LEFT_SHIFT,                       // (
    KEY_9|MOD_LEFT_SHIFT,                       // )
    KEY_SE_QUOTE|MOD_LEFT_SHIFT,                // *
    KEY_SE_PLUS,                                // +
    KEY_COMMA,                                  // ,
    KEY_SE_MINUS,                               // -
    KEY_PERIOD,                                 // .
    KEY_7|MOD_LEFT_SHIFT,                       // /
    KEY_0,                                      // 0
    KEY_1,                                      // 1
    KEY_2,                                      // 2
    KEY_3,                                      // 3
    KEY_4,                                      // 4
    KEY_5,                                      // 5
    KEY_6,                                      // 6
    KEY_7,                                      // 7
    KEY_8,                                      // 8
    KEY_9,                                      // 9
    KEY_PERIOD|MOD_LEFT_SHIFT,                  // :
    KEY_COMMA|MOD_LEFT_SHIFT,                   // ;
    KEY_SE_LT_GT,                               // <
    KEY_0|MOD_LEFT_SHIFT,                       // =
    KEY_SE_LT_GT|MOD_LEFT_SHIFT,                // >
    KEY_SE_PLUS|MOD_LEFT_SHIFT,                 // ?
    KEY_2|MOD_RIGHT_ALT,                        // @
    KEY_A|MOD_LEFT_SHIFT,                       // A
    KEY_B|MOD_LEFT_SHIFT,                       // B
    KEY_C|MOD_LEFT_SHIFT,                       // C
    KEY_D|MOD_LEFT_SHIFT,                       // D
    KEY_E|MOD_LEFT_SHIFT,                       // E
    KEY_F|MOD_LEFT_SHIFT,                       // F
    KEY_G|MOD_LEFT_SHIFT,                       // G
    KEY_H|MOD_LEFT_SHIFT,                       // H
    KEY_I|MOD_LEFT_SHIFT,                       // I
    KEY_J|MOD_LEFT_SHIFT,                       // J
    KEY_K|MOD_LEFT_SHIFT,                       // K
    KEY_L|MOD_LEFT_SHIFT,                       // L
    KEY_M|MOD_LEFT_SHIFT,                       // M
    KEY_N|MOD_LEFT_SHIFT,                       // N
    KEY_O|MOD_LEFT_SHIFT,                       // O
    KEY_P|MOD_LEFT_SHIFT,                       // P
    KEY_Q|MOD_LEFT_SHIFT,                       // Q
    KEY_R|MOD_LEFT_SHIFT,                       // R
    KEY_S|MOD_LEFT_SHIFT,                       // S
    KEY_T|MOD_LEFT_SHIFT,                       // T
    KEY_U|MOD_LEFT_SHIFT,                       // U
    KEY_V|MOD_LEFT_SHIFT,                       // V
    KEY_W|MOD_LEFT_SHIFT,                       // W
    KEY_X|MOD_LEFT_SHIFT,                       // X
    KEY_Y|MOD_LEFT_SHIFT,                       // Y
    KEY_Z|MOD_LEFT_SHIFT,                       // Z
    KEY_8|MOD_RIGHT_ALT,                        // [
    KEY_SE_PLUS|MOD_RIGHT_ALT,                  // bslash
    KEY_9|MOD_RIGHT_ALT,                        // ]
    KEY_RESERVED,                               // ^ (Dead key)
    KEY_SE_MINUS|MOD_LEFT_SHIFT,                // _
    KEY_RESERVED,                               // ` (Dead key)
    KEY_A,                                      // a
    KEY_B,                                      // b
    KEY_C,                                      // c
    KEY_D,                                      // d
    KEY_E,                                      // e
    KEY_F,                                      // f
    KEY_G,                                      // g
    KEY_H,                                      // h
    KEY_I,                                      // i
    KEY_J,                                      // j
    KEY_K,                                      // k
    KEY_L,                                      // l
    KEY_M,                                      // m
    KEY_N,                                      // n
    KEY_O,                                      // o
    KEY_P,                                      // p
    KEY_Q,                                      // q
    KEY_R,                                      // r
    KEY_S,                                      // s
    KEY_T,                                      // t
    KEY_U,                                      // u
    KEY_V,                                      // v
    KEY_W,                                      // w
    KEY_X,                                      // x
    KEY_Y,                                      // y
    KEY_Z,                                      // z
    KEY_7|MOD_RIGHT_ALT,                        // {
    KEY_SE_SECTION,                             // |
    KEY_0|MOD_RIGHT_ALT,                        // }
    KEY_RESERVED,                               // ~ (Dead key)
    KEY_RESERVED,                               // 127 - DEL
    // 7-bit ASCII codes end here
};

// Key aliases
#define KEY_FR_A            KEY_Q
#define KEY_FR_Q            KEY_A
#define KEY_FR_Z            KEY_W
#define KEY_FR_W            KEY_Z
#define KEY_FR_M            KEY_SEMICOLON
#define KEY_FR_COMMA        KEY_M
#define KEY_FR_SEMICOLON    KEY_COMMA
#define KEY_FR_COLON        KEY_PERIOD
#define KEY_FR_EXCLAMATION  KEY_SLASH
#define KEY_FR_CIRCUMFLEX   KEY_LEFT_BRACE
#define KEY_FR_DOLLAR       KEY_RIGHT_BRACE
#define KEY_FR_UGRAVE       KEY_QUOTE
#define KEY_FR_ASTERISK     KEY_BACKSLASH
#define KEY_FR_LT_GT        KEY_NON_US
#define KEY_FR_SUP2         KEY_TILDE
#define KEY_FR_CLBRKT       KEY_MINUS

static const uint16_t fr_asciimap[96]  =
{
    KEY_SPACE,                              // ' ' Space
    KEY_FR_EXCLAMATION,                     // !
    KEY_3,                                  // "
    KEY_3|MOD_RIGHT_ALT,                    // #
    KEY_FR_DOLLAR,                          // $
    KEY_FR_UGRAVE|MOD_LEFT_SHIFT,           // %
    KEY_1,                                  // &
    KEY_4,                                  // '
    KEY_5,                                  // (
    KEY_FR_CLBRKT,                          // )
    KEY_FR_ASTERISK,                        // *
    KEY_EQUAL|MOD_LEFT_SHIFT,               // +
    KEY_FR_COMMA,                           // ,
    KEY_6,                                  // -
    KEY_FR_SEMICOLON|MOD_LEFT_SHIFT,        // .
    KEY_FR_COLON|MOD_LEFT_SHIFT,            // /
    KEY_0|MOD_LEFT_SHIFT,                   // 0
    KEY_1|MOD_LEFT_SHIFT,                   // 1
    KEY_2|MOD_LEFT_SHIFT,                   // 2
    KEY_3|MOD_LEFT_SHIFT,                   // 3
    KEY_4|MOD_LEFT_SHIFT,                   // 4
    KEY_5|MOD_LEFT_SHIFT,                   // 5
    KEY_6|MOD_LEFT_SHIFT,                   // 6
    KEY_7|MOD_LEFT_SHIFT,                   // 7
    KEY_8|MOD_LEFT_SHIFT,                   // 8
    KEY_9|MOD_LEFT_SHIFT,                   // 9
    KEY_FR_COLON,                           // :
    KEY_FR_SEMICOLON,                       // ;
    KEY_FR_LT_GT,                           // <
    KEY_EQUAL,                              // =
    KEY_FR_LT_GT|MOD_LEFT_SHIFT,            // >
    KEY_FR_COMMA|MOD_LEFT_SHIFT,            // ?
    KEY_0|MOD_RIGHT_ALT,                    // @
    KEY_FR_A|MOD_LEFT_SHIFT,                // A
    KEY_B|MOD_LEFT_SHIFT,                   // B
    KEY_C|MOD_LEFT_SHIFT,                   // C
    KEY_D|MOD_LEFT_SHIFT,                   // D
    KEY_E|MOD_LEFT_SHIFT,                   // E
    KEY_F|MOD_LEFT_SHIFT,                   // F
    KEY_G|MOD_LEFT_SHIFT,                   // G
    KEY_H|MOD_LEFT_SHIFT,                   // H
    KEY_I|MOD_LEFT_SHIFT,                   // I
    KEY_J|MOD_LEFT_SHIFT,                   // J
    KEY_K|MOD_LEFT_SHIFT,                   // K
    KEY_L|MOD_LEFT_SHIFT,                   // L
    KEY_FR_M|MOD_LEFT_SHIFT,                // M
    KEY_N|MOD_LEFT_SHIFT,                   // N
    KEY_O|MOD_LEFT_SHIFT,                   // O
    KEY_P|MOD_LEFT_SHIFT,                   // P
    KEY_FR_Q|MOD_LEFT_SHIFT,                // Q
    KEY_R|MOD_LEFT_SHIFT,                   // R
    KEY_S|MOD_LEFT_SHIFT,                   // S
    KEY_T|MOD_LEFT_SHIFT,                   // T
    KEY_U|MOD_LEFT_SHIFT,                   // U
    KEY_V|MOD_LEFT_SHIFT,                   // V
    KEY_FR_W|MOD_LEFT_SHIFT,                // W
    KEY_X|MOD_LEFT_SHIFT,                   // X
    KEY_Y|MOD_LEFT_SHIFT,                   // Y
    KEY_FR_Z|MOD_LEFT_SHIFT,                // Z
    KEY_5|MOD_RIGHT_ALT,                    // [
    KEY_8|MOD_RIGHT_ALT,                    // bslash
    KEY_FR_CLBRKT|MOD_RIGHT_ALT,            // ]
    KEY_9|MOD_RIGHT_ALT,                    // ^
    KEY_8,                                  // _
    KEY_RESERVED,                           // ` (This is a dead key in the FR layout, would require ALTRGR+7 *followed by* space)
    KEY_FR_A,                               // a
    KEY_B,                                  // b
    KEY_C,                                  // c
    KEY_D,                                  // d
    KEY_E,                                  // e
    KEY_F,                                  // f
    KEY_G,                                  // g
    KEY_H,                                  // h
    KEY_I,                                  // i
    KEY_J,                                  // j
    KEY_K,                                  // k
    KEY_L,                                  // l
    KEY_FR_M,                               // m
    KEY_N,                                  // n
    KEY_O,                                  // o
    KEY_P,                                  // p
    KEY_FR_Q,                               // q
    KEY_R,                                  // r
    KEY_S,                                  // s
    KEY_T,                                  // t
    KEY_U,                                  // u
    KEY_V,                                  // v
    KEY_FR_W,                               // w
    KEY_X,                                  // x
    KEY_Y,                                  // y
    KEY_FR_Z,                               // z
    KEY_4|MOD_RIGHT_ALT,                    // {
    KEY_6|MOD_RIGHT_ALT,                    // |
    KEY_EQUAL|MOD_RIGHT_ALT,                // }
    KEY_RESERVED,                           // ~ (This is a dead key in the FR layout, would require ALTRGR+2 *followed by* space)
    KEY_RESERVED,                           // 127 - DEL
    // 7-bit ASCII codes end here
};


// Key aliases
#define KEY_BE_A            KEY_Q
#define KEY_BE_Q            KEY_A
#define KEY_BE_Z            KEY_W
#define KEY_BE_W            KEY_Z
#define KEY_BE_M            KEY_SEMICOLON
#define KEY_BE_COMMA        KEY_M
#define KEY_BE_SEMICOLON    KEY_COMMA
#define KEY_BE_COLON        KEY_PERIOD
#define KEY_BE_EQUAL        KEY_SLASH
#define KEY_BE_CIRCUMFLEX   KEY_LEFT_BRACE
#define KEY_BE_DOLLAR       KEY_RIGHT_BRACE
#define KEY_BE_UGRAVE       KEY_QUOTE
#define KEY_BE_MU           KEY_BACKSLASH
#define KEY_BE_LT_GT        KEY_NON_US
#define KEY_BE_SUP2         KEY_TILDE
#define KEY_BE_CLBRKT       KEY_MINUS
#define KEY_BE_MINUS		KEY_EQUAL

static const uint16_t be_asciimap[96] =
{
    KEY_SPACE,                              // ' ' Space
    KEY_8,                                  // !
    KEY_3,                                  // "
    KEY_3|MOD_RIGHT_ALT,                    // #
    KEY_BE_DOLLAR,                          // $
    KEY_BE_UGRAVE|MOD_LEFT_SHIFT,           // %
    KEY_1,                                  // &
    KEY_4,                                  // '
    KEY_5,                                  // (
    KEY_BE_CLBRKT,                          // )
    KEY_BE_DOLLAR|MOD_LEFT_SHIFT,           // *
    KEY_BE_EQUAL|MOD_LEFT_SHIFT,            // +
    KEY_BE_COMMA,                           // ,
    KEY_BE_MINUS,                           // -
    KEY_BE_SEMICOLON|MOD_LEFT_SHIFT,        // .
    KEY_BE_COLON|MOD_LEFT_SHIFT,            // /
    KEY_0|MOD_LEFT_SHIFT,                   // 0
    KEY_1|MOD_LEFT_SHIFT,                   // 1
    KEY_2|MOD_LEFT_SHIFT,                   // 2
    KEY_3|MOD_LEFT_SHIFT,                   // 3
    KEY_4|MOD_LEFT_SHIFT,                   // 4
    KEY_5|MOD_LEFT_SHIFT,                   // 5
    KEY_6|MOD_LEFT_SHIFT,                   // 6
    KEY_7|MOD_LEFT_SHIFT,                   // 7
    KEY_8|MOD_LEFT_SHIFT,                   // 8
    KEY_9|MOD_LEFT_SHIFT,                   // 9
    KEY_BE_COLON,                           // :
    KEY_BE_SEMICOLON,                       // ;
    KEY_BE_LT_GT,                           // <
    KEY_BE_EQUAL,                           // =
    KEY_BE_LT_GT|MOD_LEFT_SHIFT,            // >
    KEY_BE_COMMA|MOD_LEFT_SHIFT,            // ?
    KEY_2|MOD_RIGHT_ALT,                    // @
    KEY_BE_A|MOD_LEFT_SHIFT,                // A
    KEY_B|MOD_LEFT_SHIFT,                   // B
    KEY_C|MOD_LEFT_SHIFT,                   // C
    KEY_D|MOD_LEFT_SHIFT,                   // D
    KEY_E|MOD_LEFT_SHIFT,                   // E
    KEY_F|MOD_LEFT_SHIFT,                   // F
    KEY_G|MOD_LEFT_SHIFT,                   // G
    KEY_H|MOD_LEFT_SHIFT,                   // H
    KEY_I|MOD_LEFT_SHIFT,                   // I
    KEY_J|MOD_LEFT_SHIFT,                   // J
    KEY_K|MOD_LEFT_SHIFT,                   // K
    KEY_L|MOD_LEFT_SHIFT,                   // L
    KEY_BE_M|MOD_LEFT_SHIFT,                // M
    KEY_N|MOD_LEFT_SHIFT,                   // N
    KEY_O|MOD_LEFT_SHIFT,                   // O
    KEY_P|MOD_LEFT_SHIFT,                   // P
    KEY_BE_Q|MOD_LEFT_SHIFT,                // Q
    KEY_R|MOD_LEFT_SHIFT,                   // R
    KEY_S|MOD_LEFT_SHIFT,                   // S
    KEY_T|MOD_LEFT_SHIFT,                   // T
    KEY_U|MOD_LEFT_SHIFT,                   // U
    KEY_V|MOD_LEFT_SHIFT,                   // V
    KEY_BE_W|MOD_LEFT_SHIFT,                // W
    KEY_X|MOD_LEFT_SHIFT,                   // X
    KEY_Y|MOD_LEFT_SHIFT,                   // Y
    KEY_BE_Z|MOD_LEFT_SHIFT,                // Z
    KEY_BE_CIRCUMFLEX|MOD_RIGHT_ALT,        // [
    KEY_BE_LT_GT|MOD_RIGHT_ALT,             // bslash
    KEY_BE_DOLLAR|MOD_RIGHT_ALT,            // ]
    KEY_6|MOD_RIGHT_ALT,                    // ^
    KEY_BE_MINUS|MOD_LEFT_SHIFT,            // _
    KEY_RESERVED,                           // ` (This is a dead key in the BE layout, would require ALTRGR+MU *followed by* space)
    KEY_BE_A,                               // a
    KEY_B,                                  // b
    KEY_C,                                  // c
    KEY_D,                                  // d
    KEY_E,                                  // e
    KEY_F,                                  // f
    KEY_G,                                  // g
    KEY_H,                                  // h
    KEY_I,                                  // i
    KEY_J,                                  // j
    KEY_K,                                  // k
    KEY_L,                                  // l
    KEY_BE_M,                               // m
    KEY_N,                                  // n
    KEY_O,                                  // o
    KEY_P,                                  // p
    KEY_BE_Q,                               // q
    KEY_R,                                  // r
    KEY_S,                                  // s
    KEY_T,                                  // t
    KEY_U,                                  // u
    KEY_V,                                  // v
    KEY_BE_W,                               // w
    KEY_X,                                  // x
    KEY_Y,                                  // y
    KEY_BE_Z,                               // z
    KEY_9|MOD_RIGHT_ALT,                    // {
    KEY_1|MOD_RIGHT_ALT,                    // |
    KEY_0|MOD_RIGHT_ALT,                    // }
    KEY_RESERVED,                           // ~ (This is a dead key in the BE layout, would require ALTRGR+= *followed by* space)
    KEY_RESERVED,                           // 127 - DEL
    // 7-bit ASCII codes end here
};

// Key aliases
#define KEY_CH_Y            KEY_Z
#define KEY_CH_Z            KEY_Y
#define KEY_CH_ACCENT       KEY_EQUAL
#define KEY_CH_UE           KEY_LEFT_BRACE
#define KEY_CH_OE           KEY_SEMICOLON
#define KEY_CH_AE           KEY_QUOTE
#define KEY_CH_MINUS        KEY_SLASH
#define KEY_CH_LT_GT        KEY_NON_US
#define KEY_CH_QUOTE        KEY_MINUS
#define KEY_CH_UMLAUT       KEY_RIGHT_BRACE
#define KEY_CH_DOLLAR       KEY_BACKSLASH
#define KEY_CH_SECTION      KEY_TILDE

static const uint16_t ch_asciimap[96] =
{
    KEY_SPACE,                      // ' ' Space
    KEY_CH_UMLAUT|MOD_LEFT_SHIFT,   // !
    KEY_2|MOD_LEFT_SHIFT,           // "
    KEY_3|MOD_RIGHT_ALT,            // #
    KEY_CH_DOLLAR,                  // $
    KEY_5|MOD_LEFT_SHIFT,           // %
    KEY_6|MOD_LEFT_SHIFT,           // &
    KEY_CH_QUOTE,                   // '
    KEY_8|MOD_LEFT_SHIFT,           // (
    KEY_9|MOD_LEFT_SHIFT,           // )
    KEY_3|MOD_LEFT_SHIFT,           // *
    KEY_1|MOD_LEFT_SHIFT,           // +
    KEY_COMMA,                      // ,
    KEY_CH_MINUS,                   // -
    KEY_PERIOD,                     // .
    KEY_7|MOD_LEFT_SHIFT,           // /
    KEY_0,                          // 0
    KEY_1,                          // 1
    KEY_2,                          // 2
    KEY_3,                          // 3
    KEY_4,                          // 4
    KEY_5,                          // 5
    KEY_6,                          // 6
    KEY_7,                          // 7
    KEY_8,                          // 8
    KEY_9,                          // 9
    KEY_PERIOD|MOD_LEFT_SHIFT,      // :
    KEY_COMMA|MOD_LEFT_SHIFT,       // ;
    KEY_CH_LT_GT,                   // <
    KEY_0|MOD_LEFT_SHIFT,           // =
    KEY_CH_LT_GT|MOD_LEFT_SHIFT,    // >
    KEY_CH_QUOTE|MOD_LEFT_SHIFT,    // ?
    KEY_2|MOD_RIGHT_ALT,            // @
    KEY_A|MOD_LEFT_SHIFT,           // A
    KEY_B|MOD_LEFT_SHIFT,           // B
    KEY_C|MOD_LEFT_SHIFT,           // C
    KEY_D|MOD_LEFT_SHIFT,           // D
    KEY_E|MOD_LEFT_SHIFT,           // E
    KEY_F|MOD_LEFT_SHIFT,           // F
    KEY_G|MOD_LEFT_SHIFT,           // G
    KEY_H|MOD_LEFT_SHIFT,           // H
    KEY_I|MOD_LEFT_SHIFT,           // I
    KEY_J|MOD_LEFT_SHIFT,           // J
    KEY_K|MOD_LEFT_SHIFT,           // K
    KEY_L|MOD_LEFT_SHIFT,           // L
    KEY_M|MOD_LEFT_SHIFT,           // M
    KEY_N|MOD_LEFT_SHIFT,           // N
    KEY_O|MOD_LEFT_SHIFT,           // O
    KEY_P|MOD_LEFT_SHIFT,           // P
    KEY_Q|MOD_LEFT_SHIFT,           // Q
    KEY_R|MOD_LEFT_SHIFT,           // R
    KEY_S|MOD_LEFT_SHIFT,           // S
    KEY_T|MOD_LEFT_SHIFT,           // T
    KEY_U|MOD_LEFT_SHIFT,           // U
    KEY_V|MOD_LEFT_SHIFT,           // V
    KEY_W|MOD_LEFT_SHIFT,           // W
    KEY_X|MOD_LEFT_SHIFT,           // X
    KEY_CH_Y|MOD_LEFT_SHIFT,        // Y
    KEY_CH_Z|MOD_LEFT_SHIFT,        // Z
    KEY_CH_UE|MOD_RIGHT_ALT,        // [
    KEY_CH_LT_GT|MOD_RIGHT_ALT,     // bslash
    KEY_CH_UMLAUT|MOD_RIGHT_ALT,    // ]
    KEY_RESERVED,                   // ^ (Dead key)
    KEY_CH_MINUS|MOD_LEFT_SHIFT,    // _
    KEY_RESERVED,                   // ` (Dead key)
    KEY_A,                          // a
    KEY_B,                          // b
    KEY_C,                          // c
    KEY_D,                          // d
    KEY_E,                          // e
    KEY_F,                          // f
    KEY_G,                          // g
    KEY_H,                          // h
    KEY_I,                          // i
    KEY_J,                          // j
    KEY_K,                          // k
    KEY_L,                          // l
    KEY_M,                          // m
    KEY_N,                          // n
    KEY_O,                          // o
    KEY_P,                          // p
    KEY_Q,                          // q
    KEY_R,                          // r
    KEY_S,                          // s
    KEY_T,                          // t
    KEY_U,                          // u
    KEY_V,                          // v
    KEY_W,                          // w
    KEY_X,                          // x
    KEY_CH_Y,                       // y
    KEY_CH_Z,                       // z
    KEY_CH_AE|MOD_RIGHT_ALT,        // {
    KEY_1|MOD_RIGHT_ALT,            // |
    KEY_CH_DOLLAR|MOD_RIGHT_ALT,    // }
    KEY_RESERVED,                   // ~ (Dead key)
    KEY_RESERVED,                   // 127 - DEL
    // 7-bit ASCII codes end here
};

// Key aliases
#define KEY_DE_Y		KEY_Z
#define KEY_DE_Z		KEY_Y
#define KEY_DE_SZ		KEY_MINUS
#define KEY_DE_ACCENT   KEY_EQUAL
#define KEY_DE_UE       KEY_LEFT_BRACE
#define KEY_DE_PLUS     KEY_RIGHT_BRACE
#define KEY_DE_HASHTAG2 KEY_BACKSLASH
#define KEY_DE_HASHTAG  KEY_NON_US_NUM
#define KEY_DE_OE       KEY_SEMICOLON
#define KEY_DE_AE       KEY_QUOTE
#define KEY_DE_DACH     KEY_TILDE
#define KEY_DE_MINUS    KEY_SLASH
#define KEY_DE_SMALLER  KEY_NON_US

static const uint16_t de_asciimap[96] =
{
	KEY_SPACE,		   		// ' ' Space
	KEY_1|MOD_LEFT_SHIFT,			// !
	KEY_2|MOD_LEFT_SHIFT,			// "
	KEY_DE_HASHTAG,		    		// #
	KEY_4|MOD_LEFT_SHIFT,    		// $
	KEY_5|MOD_LEFT_SHIFT,    		// %
	KEY_6|MOD_LEFT_SHIFT,    		// &
	KEY_DE_HASHTAG|MOD_LEFT_SHIFT,     // '
	KEY_8|MOD_LEFT_SHIFT,    		// (
	KEY_9|MOD_LEFT_SHIFT,    		// )
	KEY_DE_PLUS|MOD_LEFT_SHIFT,    	// *
	KEY_DE_PLUS,    					// +
	KEY_COMMA,          	// ,
	KEY_DE_MINUS,          	// -
	KEY_PERIOD,          	// .
	KEY_7|MOD_LEFT_SHIFT,   // /
	KEY_0,          		// 0
	KEY_1,          		// 1
	KEY_2,          		// 2
	KEY_3,          		// 3
	KEY_4,          		// 4
	KEY_5,          		// 5
	KEY_6,          		// 6
	KEY_7,          		// 7
	KEY_8,          		// 8
	KEY_9,          		// 9
	KEY_PERIOD|MOD_LEFT_SHIFT,	// :
	KEY_COMMA|MOD_LEFT_SHIFT,   // ;
	KEY_DE_SMALLER,      			// <
	KEY_0|MOD_LEFT_SHIFT,          	// =
	KEY_DE_SMALLER|MOD_LEFT_SHIFT,      // >
	KEY_DE_SZ|MOD_LEFT_SHIFT,      	// ?
	KEY_Q|MOD_RIGHT_ALT,      		// @
	KEY_A|MOD_LEFT_SHIFT,      		// A
	KEY_B|MOD_LEFT_SHIFT,      		// B
	KEY_C|MOD_LEFT_SHIFT,      		// C
	KEY_D|MOD_LEFT_SHIFT,      		// D
	KEY_E|MOD_LEFT_SHIFT,      		// E
	KEY_F|MOD_LEFT_SHIFT,      		// F
	KEY_G|MOD_LEFT_SHIFT,      		// G
	KEY_H|MOD_LEFT_SHIFT,      		// H
	KEY_I|MOD_LEFT_SHIFT,      		// I
	KEY_J|MOD_LEFT_SHIFT,      		// J
	KEY_K|MOD_LEFT_SHIFT,      		// K
	KEY_L|MOD_LEFT_SHIFT,      		// L
	KEY_M|MOD_LEFT_SHIFT,      		// M
	KEY_N|MOD_LEFT_SHIFT,      		// N
	KEY_O|MOD_LEFT_SHIFT,      		// O
	KEY_P|MOD_LEFT_SHIFT,      		// P
	KEY_Q|MOD_LEFT_SHIFT,      		// Q
	KEY_R|MOD_LEFT_SHIFT,      		// R
	KEY_S|MOD_LEFT_SHIFT,      		// S
	KEY_T|MOD_LEFT_SHIFT,      		// T
	KEY_U|MOD_LEFT_SHIFT,      		// U
	KEY_V|MOD_LEFT_SHIFT,      		// V
	KEY_W|MOD_LEFT_SHIFT,      		// W
	KEY_X|MOD_LEFT_SHIFT,      		// X
	KEY_DE_Y|MOD_LEFT_SHIFT,      		// Y
	KEY_DE_Z|MOD_LEFT_SHIFT,      		// Z
	KEY_8|MOD_RIGHT_ALT,	         // [
	KEY_DE_SZ|MOD_RIGHT_ALT,          // bslash
	KEY_9|MOD_RIGHT_ALT,        	// ]
	KEY_6|MOD_LEFT_SHIFT,    		// ^
	KEY_DE_MINUS|MOD_LEFT_SHIFT,    	// _
	KEY_DE_ACCENT|MOD_LEFT_SHIFT,      // `
	KEY_A,          		// a
	KEY_B,          		// b
	KEY_C,          		// c
	KEY_D,          		// d
	KEY_E,          		// e
	KEY_F,          		// f
	KEY_G,          		// g
	KEY_H,          		// h
	KEY_I,          		// i
	KEY_J,          		// j
	KEY_K,         		 	// k
	KEY_L,          		// l
	KEY_M,          		// m
	KEY_N,          		// n
	KEY_O,          		// o
	KEY_P,          		// p
	KEY_Q,          		// q
	KEY_R,          		// r
	KEY_S,          		// s
	KEY_T,          		// t
	KEY_U,          		// u
	KEY_V,          		// v
	KEY_W,          		// w
	KEY_X,          		// x
	KEY_DE_Y,          		// y
	KEY_DE_Z,          		// z
	KEY_7|MOD_RIGHT_ALT,	// {
	KEY_DE_SMALLER|MOD_RIGHT_ALT,    		// |
	KEY_0|MOD_RIGHT_ALT,	// }
	KEY_DE_PLUS|MOD_RIGHT_ALT,    	// ~
	KEY_RESERVED			// DEL
};


// Key aliases
#define KEY_IT_QUOTE        KEY_MINUS
#define KEY_IT_IGRAVE       KEY_EQUAL
#define KEY_IT_EGRAVE       KEY_LEFT_BRACE
#define KEY_IT_PLUS         KEY_RIGHT_BRACE
#define KEY_IT_UGRAVE       KEY_BACKSLASH
#define KEY_IT_OGRAVE       KEY_SEMICOLON
#define KEY_IT_AGRAVE       KEY_QUOTE
#define KEY_IT_BACKSLASH    KEY_TILDE
#define KEY_IT_MINUS        KEY_SLASH
#define KEY_IT_LT_GT        KEY_NON_US


static const uint16_t it_asciimap[96] =
{
    KEY_SPACE,                                  // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,                       // !
    KEY_2|MOD_LEFT_SHIFT,                       // "
    KEY_IT_AGRAVE|MOD_RIGHT_ALT,                // #
    KEY_4|MOD_LEFT_SHIFT,                       // $
    KEY_5|MOD_LEFT_SHIFT,                       // %
    KEY_6|MOD_LEFT_SHIFT,                       // &
    KEY_IT_QUOTE,                               // '            
    KEY_8|MOD_LEFT_SHIFT,                       // (
    KEY_9|MOD_LEFT_SHIFT,                       // )
    KEY_IT_PLUS|MOD_LEFT_SHIFT,                 // *
    KEY_IT_PLUS,                                // +
    KEY_COMMA,                                  // ,
    KEY_IT_MINUS,                               // -
    KEY_PERIOD,                                 // .
    KEY_7|MOD_LEFT_SHIFT,                       // /
    KEY_0,                                      // 0
    KEY_1,                                      // 1
    KEY_2,                                      // 2
    KEY_3,                                      // 3
    KEY_4,                                      // 4
    KEY_5,                                      // 5
    KEY_6,                                      // 6
    KEY_7,                                      // 7
    KEY_8,                                      // 8
    KEY_9,                                      // 9
    KEY_PERIOD|MOD_LEFT_SHIFT,                  // :
    KEY_COMMA|MOD_LEFT_SHIFT,                   // ;
    KEY_IT_LT_GT,                               // <
    KEY_0|MOD_LEFT_SHIFT,                       // =
    KEY_IT_LT_GT|MOD_LEFT_SHIFT,                // >
    KEY_IT_QUOTE|MOD_LEFT_SHIFT,                // ?
    KEY_IT_OGRAVE|MOD_RIGHT_ALT,                // @
    KEY_A|MOD_LEFT_SHIFT,                       // A
    KEY_B|MOD_LEFT_SHIFT,                       // B
    KEY_C|MOD_LEFT_SHIFT,                       // C
    KEY_D|MOD_LEFT_SHIFT,                       // D
    KEY_E|MOD_LEFT_SHIFT,                       // E
    KEY_F|MOD_LEFT_SHIFT,                       // F
    KEY_G|MOD_LEFT_SHIFT,                       // G
    KEY_H|MOD_LEFT_SHIFT,                       // H
    KEY_I|MOD_LEFT_SHIFT,                       // I
    KEY_J|MOD_LEFT_SHIFT,                       // J
    KEY_K|MOD_LEFT_SHIFT,                       // K
    KEY_L|MOD_LEFT_SHIFT,                       // L
    KEY_M|MOD_LEFT_SHIFT,                       // M
    KEY_N|MOD_LEFT_SHIFT,                       // N
    KEY_O|MOD_LEFT_SHIFT,                       // O
    KEY_P|MOD_LEFT_SHIFT,                       // P
    KEY_Q|MOD_LEFT_SHIFT,                       // Q
    KEY_R|MOD_LEFT_SHIFT,                       // R
    KEY_S|MOD_LEFT_SHIFT,                       // S
    KEY_T|MOD_LEFT_SHIFT,                       // T
    KEY_U|MOD_LEFT_SHIFT,                       // U
    KEY_V|MOD_LEFT_SHIFT,                       // V
    KEY_W|MOD_LEFT_SHIFT,                       // W
    KEY_X|MOD_LEFT_SHIFT,                       // X
    KEY_Y|MOD_LEFT_SHIFT,                       // Y
    KEY_Z|MOD_LEFT_SHIFT,                       // Z
    KEY_IT_EGRAVE|MOD_RIGHT_ALT,                // [
    KEY_IT_BACKSLASH,                           // bslash
    KEY_IT_PLUS|MOD_RIGHT_ALT,                  // ]
    KEY_IT_IGRAVE|MOD_LEFT_SHIFT,               // ^
    KEY_IT_MINUS|MOD_LEFT_SHIFT,                // _
    KEY_IT_QUOTE|MOD_RIGHT_ALT,                 // ` (I think this only works on Linux)
    KEY_A,                                      // a
    KEY_B,                                      // b
    KEY_C,                                      // c
    KEY_D,                                      // d
    KEY_E,                                      // e
    KEY_F,                                      // f
    KEY_G,                                      // g
    KEY_H,                                      // h
    KEY_I,                                      // i
    KEY_J,                                      // j
    KEY_K,                                      // k
    KEY_L,                                      // l
    KEY_M,                                      // m
    KEY_N,                                      // n
    KEY_O,                                      // o
    KEY_P,                                      // p
    KEY_Q,                                      // q
    KEY_R,                                      // r
    KEY_S,                                      // s
    KEY_T,                                      // t
    KEY_U,                                      // u
    KEY_V,                                      // v
    KEY_W,                                      // w
    KEY_X,                                      // x
    KEY_Y,                                      // y
    KEY_Z,                                      // z
    KEY_IT_EGRAVE|MOD_LEFT_SHIFT|MOD_RIGHT_ALT, // {
    KEY_IT_BACKSLASH|MOD_LEFT_SHIFT,            // |
    KEY_IT_PLUS|MOD_LEFT_SHIFT|MOD_RIGHT_ALT,   // }
    KEY_IT_IGRAVE|MOD_RIGHT_ALT,                // ~ (Linux only: This works in X11, in console it's KEY_0|MOD_RIGHT_ALT)
    KEY_RESERVED,                               // 127 - DEL
};


// Key aliases
#define KEY_JP_COLON        KEY_NON_US
#define KEY_JP_AT           KEY_NON_US
#define KEY_JP_CARET        KEY_NON_US

#define KEY_JP_HANZEN       KEY_TILDE
#define KEY_JP_KANJI        KEY_TILDE // Alias
#define KEY_JP_BACKSLASH    KEY_INTERNATIONAL1
#define KEY_JP_HIRAGANA     KEY_INTERNATIONAL2
#define KEY_JP_YEN          KEY_INTERNATIONAL3
#define KEY_JP_HENKAN       KEY_INTERNATIONAL4
#define KEY_JP_MUHENKAN     KEY_INTERNATIONAL5

static const uint16_t jp_asciimap[96] =
{
    KEY_SPACE,                          // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,               // !
    KEY_2|MOD_LEFT_SHIFT,               // "
    KEY_3|MOD_LEFT_SHIFT,               // #
    KEY_4|MOD_LEFT_SHIFT,               // $
    KEY_5|MOD_LEFT_SHIFT,               // %
    KEY_6|MOD_LEFT_SHIFT,               // &
    KEY_7|MOD_LEFT_SHIFT,               // '
    KEY_8|MOD_LEFT_SHIFT,               // (
    KEY_9|MOD_LEFT_SHIFT,               // )
    KEY_QUOTE|MOD_LEFT_SHIFT,           // *
    KEY_SEMICOLON|MOD_LEFT_SHIFT,       // +
    KEY_COMMA,                          // ,
    KEY_MINUS,                          // -
    KEY_PERIOD,                         // .
    KEY_SLASH,                          // /
    KEY_0,                              // 0
    KEY_1,                              // 1
    KEY_2,                              // 2
    KEY_3,                              // 3
    KEY_4,                              // 4
    KEY_5,                              // 5
    KEY_6,                              // 6
    KEY_7,                              // 7
    KEY_8,                              // 8
    KEY_9,                              // 9
    KEY_QUOTE,                          // :
    KEY_SEMICOLON,                      // ;
    KEY_COMMA|MOD_LEFT_SHIFT,           // <
    KEY_MINUS|MOD_LEFT_SHIFT,           // =
    KEY_PERIOD|MOD_LEFT_SHIFT,          // >
    KEY_SLASH|MOD_LEFT_SHIFT,           // ?
    KEY_LEFT_BRACE,                     // @ 
    KEY_A|MOD_LEFT_SHIFT,               // A
    KEY_B|MOD_LEFT_SHIFT,               // B
    KEY_C|MOD_LEFT_SHIFT,               // C
    KEY_D|MOD_LEFT_SHIFT,               // D
    KEY_E|MOD_LEFT_SHIFT,               // E
    KEY_F|MOD_LEFT_SHIFT,               // F
    KEY_G|MOD_LEFT_SHIFT,               // G
    KEY_H|MOD_LEFT_SHIFT,               // H
    KEY_I|MOD_LEFT_SHIFT,               // I
    KEY_J|MOD_LEFT_SHIFT,               // J
    KEY_K|MOD_LEFT_SHIFT,               // K
    KEY_L|MOD_LEFT_SHIFT,               // L
    KEY_M|MOD_LEFT_SHIFT,               // M
    KEY_N|MOD_LEFT_SHIFT,               // N
    KEY_O|MOD_LEFT_SHIFT,               // O
    KEY_P|MOD_LEFT_SHIFT,               // P
    KEY_Q|MOD_LEFT_SHIFT,               // Q
    KEY_R|MOD_LEFT_SHIFT,               // R
    KEY_S|MOD_LEFT_SHIFT,               // S
    KEY_T|MOD_LEFT_SHIFT,               // T
    KEY_U|MOD_LEFT_SHIFT,               // U
    KEY_V|MOD_LEFT_SHIFT,               // V
    KEY_W|MOD_LEFT_SHIFT,               // W
    KEY_X|MOD_LEFT_SHIFT,               // X
    KEY_Y|MOD_LEFT_SHIFT,               // Y
    KEY_Z|MOD_LEFT_SHIFT,               // Z
    KEY_RIGHT_BRACE,                    // [
    KEY_JP_BACKSLASH,                   // bslash
    KEY_BACKSLASH,                      // ]
    KEY_EQUAL,                          // ^
    KEY_JP_BACKSLASH|MOD_LEFT_SHIFT,    // _
    KEY_LEFT_BRACE|MOD_LEFT_SHIFT,      // `
    KEY_A,                              // a
    KEY_B,                              // b
    KEY_C,                              // c
    KEY_D,                              // d
    KEY_E,                              // e
    KEY_F,                              // f
    KEY_G,                              // g
    KEY_H,                              // h
    KEY_I,                              // i
    KEY_J,                              // j
    KEY_K,                              // k
    KEY_L,                              // l
    KEY_M,                              // m
    KEY_N,                              // n
    KEY_O,                              // o
    KEY_P,                              // p
    KEY_Q,                              // q
    KEY_R,                              // r
    KEY_S,                              // s
    KEY_T,                              // t
    KEY_U,                              // u
    KEY_V,                              // v
    KEY_W,                              // w
    KEY_X,                              // x
    KEY_Y,                              // y
    KEY_Z,                              // z
    KEY_RIGHT_BRACE|MOD_LEFT_SHIFT,     // {
    KEY_JP_YEN|MOD_LEFT_SHIFT,          // |
    KEY_BACKSLASH|MOD_LEFT_SHIFT,       // }
    KEY_EQUAL|MOD_LEFT_SHIFT,           // ~
    KEY_RESERVED,                       // 127 - DEL
    // 7-bit ASCII codes end here
};


// Key aliases
#define KEY_NO_PIPE         KEY_TILDE
#define KEY_NO_PLUS         KEY_MINUS       // LOL! :)
#define KEY_NO_BACKSLASH    KEY_EQUAL
#define KEY_NO_ACIRCLE      KEY_LEFT_BRACE
#define KEY_NO_ACCENTS2     KEY_RIGHT_BRACE
#define KEY_NO_AE           KEY_QUOTE
#define KEY_NO_OSLASH       KEY_SEMICOLON
#define KEY_NO_QUOTE        KEY_BACKSLASH
#define KEY_NO_MINUS        KEY_SLASH
#define KEY_NO_LT_GT        KEY_NON_US

static const uint16_t no_asciimap[96] =
{
    KEY_SPACE,                                  // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,                       // !
    KEY_2|MOD_LEFT_SHIFT,                       // "
    KEY_3|MOD_LEFT_SHIFT,                       // #
    KEY_4|MOD_RIGHT_ALT,                        // $
    KEY_5|MOD_LEFT_SHIFT,                       // %
    KEY_6|MOD_LEFT_SHIFT,                       // &
    KEY_NO_QUOTE,                               // '
    KEY_8|MOD_LEFT_SHIFT,                       // (
    KEY_9|MOD_LEFT_SHIFT,                       // )
    KEY_NO_QUOTE|MOD_LEFT_SHIFT,                // *
    KEY_NO_PLUS,                                // +
    KEY_COMMA,                                  // ,
    KEY_NO_MINUS,                               // -
    KEY_PERIOD,                                 // .
    KEY_7|MOD_LEFT_SHIFT,                       // /
    KEY_0,                                      // 0
    KEY_1,                                      // 1
    KEY_2,                                      // 2
    KEY_3,                                      // 3
    KEY_4,                                      // 4
    KEY_5,                                      // 5
    KEY_6,                                      // 6
    KEY_7,                                      // 7
    KEY_8,                                      // 8
    KEY_9,                                      // 9
    KEY_PERIOD|MOD_LEFT_SHIFT,                  // :
    KEY_COMMA|MOD_LEFT_SHIFT,                   // ;
    KEY_NO_LT_GT,                               // <
    KEY_0|MOD_LEFT_SHIFT,                       // =
    KEY_NO_LT_GT|MOD_LEFT_SHIFT,                // >
    KEY_NO_PLUS|MOD_LEFT_SHIFT,                 // ?
    KEY_2|MOD_RIGHT_ALT,                        // @
    KEY_A|MOD_LEFT_SHIFT,                       // A
    KEY_B|MOD_LEFT_SHIFT,                       // B
    KEY_C|MOD_LEFT_SHIFT,                       // C
    KEY_D|MOD_LEFT_SHIFT,                       // D
    KEY_E|MOD_LEFT_SHIFT,                       // E
    KEY_F|MOD_LEFT_SHIFT,                       // F
    KEY_G|MOD_LEFT_SHIFT,                       // G
    KEY_H|MOD_LEFT_SHIFT,                       // H
    KEY_I|MOD_LEFT_SHIFT,                       // I
    KEY_J|MOD_LEFT_SHIFT,                       // J
    KEY_K|MOD_LEFT_SHIFT,                       // K
    KEY_L|MOD_LEFT_SHIFT,                       // L
    KEY_M|MOD_LEFT_SHIFT,                       // M
    KEY_N|MOD_LEFT_SHIFT,                       // N
    KEY_O|MOD_LEFT_SHIFT,                       // O
    KEY_P|MOD_LEFT_SHIFT,                       // P
    KEY_Q|MOD_LEFT_SHIFT,                       // Q
    KEY_R|MOD_LEFT_SHIFT,                       // R
    KEY_S|MOD_LEFT_SHIFT,                       // S
    KEY_T|MOD_LEFT_SHIFT,                       // T
    KEY_U|MOD_LEFT_SHIFT,                       // U
    KEY_V|MOD_LEFT_SHIFT,                       // V
    KEY_W|MOD_LEFT_SHIFT,                       // W
    KEY_X|MOD_LEFT_SHIFT,                       // X
    KEY_Y|MOD_LEFT_SHIFT,                       // Y
    KEY_Z|MOD_LEFT_SHIFT,                       // Z
    KEY_8|MOD_RIGHT_ALT,                        // [
    KEY_NO_BACKSLASH,                           // bslash
    KEY_9|MOD_RIGHT_ALT,                        // ]
    KEY_RESERVED,                               // ^ (Dead key)
    KEY_NO_MINUS|MOD_LEFT_SHIFT,                // _
    KEY_RESERVED,                               // ` (Dead key)
    KEY_A,                                      // a
    KEY_B,                                      // b
    KEY_C,                                      // c
    KEY_D,                                      // d
    KEY_E,                                      // e
    KEY_F,                                      // f
    KEY_G,                                      // g
    KEY_H,                                      // h
    KEY_I,                                      // i
    KEY_J,                                      // j
    KEY_K,                                      // k
    KEY_L,                                      // l
    KEY_M,                                      // m
    KEY_N,                                      // n
    KEY_O,                                      // o
    KEY_P,                                      // p
    KEY_Q,                                      // q
    KEY_R,                                      // r
    KEY_S,                                      // s
    KEY_T,                                      // t
    KEY_U,                                      // u
    KEY_V,                                      // v
    KEY_W,                                      // w
    KEY_X,                                      // x
    KEY_Y,                                      // y
    KEY_Z,                                      // z
    KEY_7|MOD_RIGHT_ALT,                        // {
    KEY_NO_PIPE,                                // |
    KEY_0|MOD_RIGHT_ALT,                        // }
    KEY_RESERVED,                               // ~ (Dead key)
    KEY_RESERVED,                               // 127 - DEL
    // 7-bit ASCII codes end here
};


// Key aliases
#define KEY_PT_QUOTE        KEY_MINUS
#define KEY_PT_GUILLS       KEY_EQUAL
#define KEY_PT_PLUS         KEY_LEFT_BRACE
#define KEY_PT_ACCENTS1     KEY_RIGHT_BRACE
#define KEY_PT_ACCENTS2     KEY_BACKSLASH
#define KEY_PT_CEDILLA      KEY_SEMICOLON
#define KEY_PT_AO           KEY_QUOTE
#define KEY_PT_BACKSLASH    KEY_TILDE
#define KEY_PT_LT_GT        KEY_NON_US
#define KEY_PT_MINUS        KEY_SLASH

static const uint16_t pt_asciimap[96] =
{
    KEY_SPACE,                          // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,               // !
    KEY_2|MOD_LEFT_SHIFT,               // "
    KEY_3|MOD_LEFT_SHIFT,               // #
    KEY_4|MOD_LEFT_SHIFT,               // $
    KEY_5|MOD_LEFT_SHIFT,               // %
    KEY_6|MOD_LEFT_SHIFT,               // &
    KEY_PT_QUOTE,                       // '
    KEY_8|MOD_LEFT_SHIFT,               // (
    KEY_9|MOD_LEFT_SHIFT,               // )
    KEY_PT_PLUS|MOD_LEFT_SHIFT,         // *
    KEY_PT_PLUS,                        // +
    KEY_COMMA,                          // ,
    KEY_PT_MINUS,                       // -
    KEY_PERIOD,                         // .
    KEY_7|MOD_LEFT_SHIFT,               // /
    KEY_0,                              // 0
    KEY_1,                              // 1
    KEY_2,                              // 2
    KEY_3,                              // 3
    KEY_4,                              // 4
    KEY_5,                              // 5
    KEY_6,                              // 6
    KEY_7,                              // 7
    KEY_8,                              // 8
    KEY_9,                              // 9
    KEY_PERIOD|MOD_LEFT_SHIFT,          // :
    KEY_COMMA|MOD_LEFT_SHIFT,           // ;
    KEY_PT_LT_GT,                       // <
    KEY_0|MOD_LEFT_SHIFT,               // =
    KEY_PT_LT_GT|MOD_LEFT_SHIFT,        // >
    KEY_PT_QUOTE|MOD_LEFT_SHIFT,        // ?
    KEY_2|MOD_RIGHT_ALT,                // @
    KEY_A|MOD_LEFT_SHIFT,               // A
    KEY_B|MOD_LEFT_SHIFT,               // B
    KEY_C|MOD_LEFT_SHIFT,               // C
    KEY_D|MOD_LEFT_SHIFT,               // D
    KEY_E|MOD_LEFT_SHIFT,               // E
    KEY_F|MOD_LEFT_SHIFT,               // F
    KEY_G|MOD_LEFT_SHIFT,               // G
    KEY_H|MOD_LEFT_SHIFT,               // H
    KEY_I|MOD_LEFT_SHIFT,               // I
    KEY_J|MOD_LEFT_SHIFT,               // J
    KEY_K|MOD_LEFT_SHIFT,               // K
    KEY_L|MOD_LEFT_SHIFT,               // L
    KEY_M|MOD_LEFT_SHIFT,               // M
    KEY_N|MOD_LEFT_SHIFT,               // N
    KEY_O|MOD_LEFT_SHIFT,               // O
    KEY_P|MOD_LEFT_SHIFT,               // P
    KEY_Q|MOD_LEFT_SHIFT,               // Q
    KEY_R|MOD_LEFT_SHIFT,               // R
    KEY_S|MOD_LEFT_SHIFT,               // S
    KEY_T|MOD_LEFT_SHIFT,               // T
    KEY_U|MOD_LEFT_SHIFT,               // U
    KEY_V|MOD_LEFT_SHIFT,               // V
    KEY_W|MOD_LEFT_SHIFT,               // W
    KEY_X|MOD_LEFT_SHIFT,               // X
    KEY_Y|MOD_LEFT_SHIFT,               // Y
    KEY_Z|MOD_LEFT_SHIFT,               // Z
    KEY_8|MOD_RIGHT_ALT,                // [
    KEY_PT_BACKSLASH,                   // bslash
    KEY_9|MOD_RIGHT_ALT,                // ]
    KEY_RESERVED,                       // ^ (Dead key)
    KEY_PT_MINUS|MOD_LEFT_SHIFT,        // _
    KEY_RESERVED,                       // ` (Dead key)
    KEY_A,                              // a
    KEY_B,                              // b
    KEY_C,                              // c
    KEY_D,                              // d
    KEY_E,                              // e
    KEY_F,                              // f
    KEY_G,                              // g
    KEY_H,                              // h
    KEY_I,                              // i
    KEY_J,                              // j
    KEY_K,                              // k
    KEY_L,                              // l
    KEY_M,                              // m
    KEY_N,                              // n
    KEY_O,                              // o
    KEY_P,                              // p
    KEY_Q,                              // q
    KEY_R,                              // r
    KEY_S,                              // s
    KEY_T,                              // t
    KEY_U,                              // u
    KEY_V,                              // v
    KEY_W,                              // w
    KEY_X,                              // x
    KEY_Y,                              // y
    KEY_Z,                              // z
    KEY_7|MOD_RIGHT_ALT,                // {
    KEY_PT_BACKSLASH|MOD_LEFT_SHIFT,    // |
    KEY_0|MOD_RIGHT_ALT,                // }
    KEY_RESERVED,                       // ~ (Dead key)
    KEY_RESERVED,                       // 127 - DEL
    // 7-bit ASCII codes end here
};

// Key aliases
#define KEY_ES_AO           KEY_TILDE
#define KEY_ES_QUOTE        KEY_MINUS
#define KEY_ES_INVMARKS     KEY_EQUAL
#define KEY_ES_CIRCUMFLEX   KEY_LEFT_BRACE
#define KEY_ES_PLUS         KEY_RIGHT_BRACE
#define KEY_ES_NTILDE       KEY_SEMICOLON
#define KEY_ES_UMLAUT       KEY_QUOTE
#define KEY_ES_CEDILLA      KEY_BACKSLASH
#define KEY_ES_MINUS        KEY_SLASH
#define KEY_ES_LT_GT        KEY_NON_US

static const uint16_t es_asciimap[96] =
{
    KEY_SPACE,                      // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,           // !
    KEY_2|MOD_LEFT_SHIFT,           // "
    KEY_3|MOD_RIGHT_ALT,            // #
    KEY_4|MOD_LEFT_SHIFT,           // $
    KEY_5|MOD_LEFT_SHIFT,           // %
    KEY_6|MOD_LEFT_SHIFT,           // &
    KEY_ES_QUOTE,                   // '
    KEY_8|MOD_LEFT_SHIFT,           // (
    KEY_9|MOD_LEFT_SHIFT,           // )
    KEY_ES_PLUS|MOD_LEFT_SHIFT,     // *
    KEY_ES_PLUS,                    // +
    KEY_COMMA,                      // ,
    KEY_ES_MINUS,                   // -
    KEY_PERIOD,                     // .
    KEY_7|MOD_LEFT_SHIFT,           // /
    KEY_0,                          // 0
    KEY_1,                          // 1
    KEY_2,                          // 2
    KEY_3,                          // 3
    KEY_4,                          // 4
    KEY_5,                          // 5
    KEY_6,                          // 6
    KEY_7,                          // 7
    KEY_8,                          // 8
    KEY_9,                          // 9
    KEY_PERIOD|MOD_LEFT_SHIFT,      // :
    KEY_COMMA|MOD_LEFT_SHIFT,       // ;
    KEY_ES_LT_GT,                   // <
    KEY_0|MOD_LEFT_SHIFT,           // =
    KEY_ES_LT_GT|MOD_LEFT_SHIFT,    // >
    KEY_ES_QUOTE|MOD_LEFT_SHIFT,    // ?
    KEY_2|MOD_RIGHT_ALT,            // @
    KEY_A|MOD_LEFT_SHIFT,           // A
    KEY_B|MOD_LEFT_SHIFT,           // B
    KEY_C|MOD_LEFT_SHIFT,           // C
    KEY_D|MOD_LEFT_SHIFT,           // D
    KEY_E|MOD_LEFT_SHIFT,           // E
    KEY_F|MOD_LEFT_SHIFT,           // F
    KEY_G|MOD_LEFT_SHIFT,           // G
    KEY_H|MOD_LEFT_SHIFT,           // H
    KEY_I|MOD_LEFT_SHIFT,           // I
    KEY_J|MOD_LEFT_SHIFT,           // J
    KEY_K|MOD_LEFT_SHIFT,           // K
    KEY_L|MOD_LEFT_SHIFT,           // L
    KEY_M|MOD_LEFT_SHIFT,           // M
    KEY_N|MOD_LEFT_SHIFT,           // N
    KEY_O|MOD_LEFT_SHIFT,           // O
    KEY_P|MOD_LEFT_SHIFT,           // P
    KEY_Q|MOD_LEFT_SHIFT,           // Q
    KEY_R|MOD_LEFT_SHIFT,           // R
    KEY_S|MOD_LEFT_SHIFT,           // S
    KEY_T|MOD_LEFT_SHIFT,           // T
    KEY_U|MOD_LEFT_SHIFT,           // U
    KEY_V|MOD_LEFT_SHIFT,           // V
    KEY_W|MOD_LEFT_SHIFT,           // W
    KEY_X|MOD_LEFT_SHIFT,           // X
    KEY_Y|MOD_LEFT_SHIFT,           // Y
    KEY_Z|MOD_LEFT_SHIFT,           // Z
    KEY_ES_CIRCUMFLEX|MOD_RIGHT_ALT,// [
    KEY_ES_AO|MOD_RIGHT_ALT,        // bslash
    KEY_ES_PLUS|MOD_RIGHT_ALT,      // ]
    KEY_RESERVED,                   // ^ (This is a dead key in the ES layout, would require SHIFT+KEY_ES_CIRCUMFLEX *followed by* space)
    KEY_ES_MINUS|MOD_LEFT_SHIFT,    // _
    KEY_RESERVED,                   // ` (This is a dead key in the ES layout, would require KEY_ES_CIRCUMFLEX *followed by* space)
    KEY_A,                          // a
    KEY_B,                          // b
    KEY_C,                          // c
    KEY_D,                          // d
    KEY_E,                          // e
    KEY_F,                          // f
    KEY_G,                          // g
    KEY_H,                          // h
    KEY_I,                          // i
    KEY_J,                          // j
    KEY_K,                          // k
    KEY_L,                          // l
    KEY_M,                          // m
    KEY_N,                          // n
    KEY_O,                          // o
    KEY_P,                          // p
    KEY_Q,                          // q
    KEY_R,                          // r
    KEY_S,                          // s
    KEY_T,                          // t
    KEY_U,                          // u
    KEY_V,                          // v
    KEY_W,                          // w
    KEY_X,                          // x
    KEY_Y,                          // y
    KEY_Z,                          // z
    KEY_ES_UMLAUT|MOD_RIGHT_ALT,    // {
    KEY_1|MOD_RIGHT_ALT,            // |
    KEY_ES_CEDILLA|MOD_RIGHT_ALT,   // }
    KEY_RESERVED,                   // ~ (This is a dead key in the ES layout, would require ALTGR+4 *followed by* space)
    KEY_RESERVED,                   // 127 - DEL
    // 7-bit ASCII codes end here
};


// Key aliases
#define KEY_UK_NEG          KEY_TILDE
#define KEY_UK_HASH         KEY_BACKSLASH
#define KEY_UK_BACKSLASH    KEY_NON_US

static const uint16_t uk_asciimap[96] =
{
    KEY_SPACE,                          // ' ' Space
    KEY_1|MOD_LEFT_SHIFT,               // !
    KEY_2|MOD_LEFT_SHIFT,               // "
    KEY_UK_HASH,                        // #
    KEY_4|MOD_LEFT_SHIFT,               // $
    KEY_5|MOD_LEFT_SHIFT,               // %
    KEY_7|MOD_LEFT_SHIFT,               // &
    KEY_QUOTE,                          // '
    KEY_9|MOD_LEFT_SHIFT,               // (
    KEY_0|MOD_LEFT_SHIFT,               // )
    KEY_8|MOD_LEFT_SHIFT,               // *
    KEY_EQUAL|MOD_LEFT_SHIFT,           // +
    KEY_COMMA,                          // ,
    KEY_MINUS,                          // -
    KEY_PERIOD,                         // .
    KEY_SLASH,                          // /
    KEY_0,                              // 0
    KEY_1,                              // 1
    KEY_2,                              // 2
    KEY_3,                              // 3
    KEY_4,                              // 4
    KEY_5,                              // 5
    KEY_6,                              // 6
    KEY_7,                              // 7
    KEY_8,                              // 8
    KEY_9,                              // 9
    KEY_SEMICOLON|MOD_LEFT_SHIFT,       // :
    KEY_SEMICOLON,                      // ;
    KEY_COMMA|MOD_LEFT_SHIFT,           // <
    KEY_EQUAL,                          // =
    KEY_PERIOD|MOD_LEFT_SHIFT,          // >
    KEY_SLASH|MOD_LEFT_SHIFT,           // ?
    KEY_QUOTE|MOD_LEFT_SHIFT,           // @
    KEY_A|MOD_LEFT_SHIFT,               // A
    KEY_B|MOD_LEFT_SHIFT,               // B
    KEY_C|MOD_LEFT_SHIFT,               // C
    KEY_D|MOD_LEFT_SHIFT,               // D
    KEY_E|MOD_LEFT_SHIFT,               // E
    KEY_F|MOD_LEFT_SHIFT,               // F
    KEY_G|MOD_LEFT_SHIFT,               // G
    KEY_H|MOD_LEFT_SHIFT,               // H
    KEY_I|MOD_LEFT_SHIFT,               // I
    KEY_J|MOD_LEFT_SHIFT,               // J
    KEY_K|MOD_LEFT_SHIFT,               // K
    KEY_L|MOD_LEFT_SHIFT,               // L
    KEY_M|MOD_LEFT_SHIFT,               // M
    KEY_N|MOD_LEFT_SHIFT,               // N
    KEY_O|MOD_LEFT_SHIFT,               // O
    KEY_P|MOD_LEFT_SHIFT,               // P
    KEY_Q|MOD_LEFT_SHIFT,               // Q
    KEY_R|MOD_LEFT_SHIFT,               // R
    KEY_S|MOD_LEFT_SHIFT,               // S
    KEY_T|MOD_LEFT_SHIFT,               // T
    KEY_U|MOD_LEFT_SHIFT,               // U
    KEY_V|MOD_LEFT_SHIFT,               // V
    KEY_W|MOD_LEFT_SHIFT,               // W
    KEY_X|MOD_LEFT_SHIFT,               // X
    KEY_Y|MOD_LEFT_SHIFT,               // Y
    KEY_Z|MOD_LEFT_SHIFT,               // Z
    KEY_LEFT_BRACE,                     // [
    KEY_UK_BACKSLASH,                   // bslash
    KEY_RIGHT_BRACE,                    // ]
    KEY_6|MOD_LEFT_SHIFT,               // ^
    KEY_MINUS|MOD_LEFT_SHIFT,           // _
    KEY_UK_NEG,                         // `
    KEY_A,                              // a
    KEY_B,                              // b
    KEY_C,                              // c
    KEY_D,                              // d
    KEY_E,                              // e
    KEY_F,                              // f
    KEY_G,                              // g
    KEY_H,                              // h
    KEY_I,                              // i
    KEY_J,                              // j
    KEY_K,                              // k
    KEY_L,                              // l
    KEY_M,                              // m
    KEY_N,                              // n
    KEY_O,                              // o
    KEY_P,                              // p
    KEY_Q,                              // q
    KEY_R,                              // r
    KEY_S,                              // s
    KEY_T,                              // t
    KEY_U,                              // u
    KEY_V,                              // v
    KEY_W,                              // w
    KEY_X,                              // x
    KEY_Y,                              // y
    KEY_Z,                              // z
    KEY_LEFT_BRACE|MOD_LEFT_SHIFT,      // {
    KEY_UK_BACKSLASH|MOD_LEFT_SHIFT,    // |
    KEY_RIGHT_BRACE|MOD_LEFT_SHIFT,     // }
    KEY_UK_HASH|MOD_LEFT_SHIFT,         // ~
    KEY_RESERVED,                       // 127 - DEL
    // 7-bit ASCII codes end here
};

static const char* name_asciimap[] =
{
	"US",
	"DK",
	"SE",
	"FR",
	"BE",
	"CH",
	"DE",
	"IT",
	"JP",
	"NO",
	"PT",
	"ES",
	"UK",
};

static const uint16_t* map_asciimap[] =
{
	us_asciimap,
	dk_asciimap,
	se_asciimap,
	fr_asciimap,
	be_asciimap,
	ch_asciimap,
	de_asciimap,
	it_asciimap,
	jp_asciimap,
	no_asciimap,
	pt_asciimap,
	es_asciimap,
	uk_asciimap,
};
